//test
// const host = 'http://tinsley.mynatapp.cc/studentReport'
// const host_backend = 'http://tinsley.mynatapp.cc/jzh'
//local
const host = 'http://192.168.0.51'
const host_backend = 'http://192.168.0.51:9789'
//production
// const host = 'https://jzh.ciiedu.cn/studentReport'
// const host_backend = 'https://jzh.ciiedu.cn/jzh'

const wxRequest = (params, url) => {
  if(params.header == undefined){
    params.header = {
      'Content-Type': 'application/json'
    }
  }

  wx.request({
    url: url,
    method: params.method || 'GET',
    data: params.data || {},
    header: params.header,
    success: (res) => {
      params.success && params.success(res)
    },
    fail: (res) => {
      params.fail && params.fail(res)
    },
    complete: (res) => {
      params.complete && params.complete(res)
    }
  })
}
// 发送验证码(CIIE)
// const sendValidateCode = (params) => wxRequest(params, host + '/weichat/ciie/send-validate-code')
// 绑定学生(CIIE)
// const postBinding = (params) => wxRequest(params, host + '/weichat/ciie/post-binding')
// 学生解绑(CIIE)
// const unBindStudent = (params) => wxRequest(params, host_backend + '/webAPI/parent/unBind')

// 检测绑定(家长汇)
const whetherBind = (params) => wxRequest(params, host_backend + '/webAPI/parent/unAuth/whetherBind/' + params.code)
// 学生报告列表(CIIE)
const viewReportV2 = (params) => wxRequest(params, host_backend + '/webAPI/student/view/reports?studentId=' + params.studentId + "&type=" + params.type)
// 报告详情(CIIE)
const reportDetailsV2 = (params) => wxRequest(params, host_backend + '/webAPI/student/view/reportDetail?studentId=' + params.studentId + "&type=" + params.type + "&reportId=" + params.reportId)
// 获取通知验证码
const getAuthenticationCode = (params) => wxRequest(params, host_backend + '/webAPI/parent/getAuthenticationCode')
// 通知验证码是否过期
const verificationNotificationCode = (params) => wxRequest(params, host_backend + '/webAPI/parent/verificationNotificationCode')
// 绑定学生  备份数据到(家长汇)
const bindStudent = (params) => wxRequest(params, host_backend + '/webAPI/parent/bindStudent')
// 学生列表(CIIE)
const listStudent = (params) => wxRequest(params, host_backend + '/webAPI/parent/findStudentsById')
// 学生解绑（家长汇）
const unBindStudentJZH = (params) => wxRequest(params, host_backend + '/webAPI/parent/unBind/')
// 初始化个人信息(家长汇)
const parentProfile = (params) => wxRequest(params, host_backend + '/webAPI/parent/parentProfile')
// 修改个人信息(家长汇)
const updateParentProfile = (params) => wxRequest(params, host_backend + '/webAPI/parent/updateParentProfile')
// 反馈列表(家长汇)
const findFeedbacksByParentId = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/findFeedbacksByParentId')
// 添加反馈(家长汇)
const addFeedback = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/feedback')
// 反馈详情(家长汇)
const feedbackDetail = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/feedbackDetail/' + params.id)
// 查找文章列表(家长汇)
const findArticles = (params) => wxRequest(params, host_backend + '/webAPI/article/findArticles')
// 获取文章详情(家长汇)
const findArticleDetial = (params) => wxRequest(params, host_backend + '/webAPI/article/findArticleDetial/' + params.id)
// 文章点赞（家长汇）
const thumbsUp = (params) => wxRequest(params, host_backend + '/webAPI/article/thumbsUp/' + params.aId)
// 文章关注（家长汇）
const weatherFollow = (params) => wxRequest(params, host_backend + '/webAPI/article/weatherFollow/' + params.aId + "/" + params.weatherFollow)
// 文章关注列表（家长汇）
const follows = (params) => wxRequest(params, host_backend + '/webAPI/parent/follows');
// 分享 +1（家长汇）
const clickShare = (params) => wxRequest(params, host_backend + '/webAPI/article/clickShare/' + params.aId);
// 反馈未回复，移除反馈（家长汇：1 成功，0 失败）
const removeById = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/removeById/' + params.fbId);
// 反馈已回复，打分（家长汇：1 - 5 分）
const scoreByFeedback = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/score/' + params.fbId + "/" + params.score);
// 发表评论（家长汇）
const comment = (params) => wxRequest(params, host_backend + '/webAPI/article/comment/' + params.aId);
// 删除评论（家长汇）
const removeComment = (params) => wxRequest(params, host_backend + '/webAPI/article/removeComment/' + params.cId);
// 首页推广（家长汇）
const findExtensions = (params) => wxRequest(params, host_backend + '/webAPI/home/findExtensions');
// 给评论点赞（家长汇）
const thumbsUpComment = (params) => wxRequest(params, host_backend + '/webAPI/article/thumbsUpComment/' + params.cId);
// 首页横幅（家长汇）
const findCarousels = (params) => wxRequest(params, host_backend + '/webAPI/Carousel/findCarousels');
// 音频播放次数
const audioPlayCount = (params) => wxRequest(params, host_backend + '/webAPI/audio/playCount/' + params.id);
// 视频播放次数
const videoPlayCount = (params) => wxRequest(params, host_backend + '/webAPI/video/playCount/' + params.id);
// 评论点赞 或 回复点赞(cId:评论id、thumbsUp：comment 或 reply 、whether：点赞（YES）、取消点赞（NO）)
const articleThumbsUpComment = (params) => wxRequest(params, host_backend + '/webAPI/article/thumbsUpComment/' + params.cId + "/" + params.thumbsUp + "/" + params.whether);
// 腾讯测试专用接口
const tencentTest = (params) => wxRequest(params, host_backend + '/webAPI/parent/unAuth/tencentTest');
// 请求后台帮助绑定（人工 Help）
const authorizationBinding = (params) => wxRequest(params, host_backend + '/webAPI/parent/unAuth/authorizationBinding/' + params.code)
// 查询一周内有多少新文章
const recentUpdateArticles = (params) => wxRequest(params, host_backend + '/webAPI/article/recentUpdateArticles')
// 报告标记为已读
const reportChangeToRead = (params) => wxRequest(params, host_backend + '/webAPI/report/reportChangeToRead')
// 查找分类下，半个月，最新文章数
const recentUpdateArticlesByType = (params) => wxRequest(params, host_backend + '/webAPI/article/recentUpdateArticlesByType/' + params.startTime + "/" + params.endTime)
// 查找回复反馈，但没看过的
const findNotCheckedFeedbacks = (params) => wxRequest(params, host_backend + '/webAPI/Feedback/findNotCheckedFeedbacks')
// 解绑历史列表
const unBindStuList = (params) => wxRequest(params, host_backend + '/webAPI/settings/unBindStuList')
// 查询是否已报名
const whetherEnroll = (params) => wxRequest(params, host_backend + '/webAPI/article/whetherEnroll/' + params.aId)
// 提交报名信息
const enroll = (params) => wxRequest(params, host_backend + '/webAPI/article/enroll')
// 收集客户端 formIds
const saveFormId = (params) => wxRequest(params, host_backend + '/webAPI/Notify/saveFormId')
// 学生在美相册列表
const studentPhotoAlbums = (params) => wxRequest(params, host_backend + '/webAPI/report/studentPhotoAlbums/' + params.sId)
// 在美图片：标记为已读
const previewStudentAlbum = (params) => wxRequest(params, host_backend + '/webAPI/report/previewStudentAlbum/' + params.ndId)

module.exports = {
  saveFormId,
  whetherBind,
  viewReportV2,
  reportDetailsV2,
  listStudent,
  unBindStudentJZH,
  bindStudent,
  parentProfile,
  updateParentProfile,
  findFeedbacksByParentId,
  addFeedback,
  feedbackDetail,
  findArticles,
  findArticleDetial,
  thumbsUp,
  weatherFollow,
  follows,
  clickShare,
  removeById,
  scoreByFeedback,
  comment,
  removeComment,
  findExtensions,
  thumbsUpComment,
  articleThumbsUpComment,
  findCarousels,
  audioPlayCount,
  videoPlayCount,
  authorizationBinding,
  tencentTest,
  recentUpdateArticles,
  getAuthenticationCode,
  verificationNotificationCode,
  reportChangeToRead,
  recentUpdateArticlesByType,
  findNotCheckedFeedbacks,
  unBindStuList,
  whetherEnroll,
  enroll,
  studentPhotoAlbums,
  previewStudentAlbum,
  host_backend
}
