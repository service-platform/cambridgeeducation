//index.js
var app = getApp()
Page({
  data: {
    serverData:[
      {title: "手把手教你选择未来职业道路",desc: "你如何看待这个世界？将来要走哪条路？医学？法律？经济还是教育？这些看似大学生才会纠结的问题..."},
      {title: "【CIIE探校】第一站：苹果园学校",desc: "在走进位于马萨诸塞州中北部菲奇堡小城的苹果园学校之前，相信大家对它的印象大概只停留在以下..."},
      {title: "【CIIE探校】走进芬威克主教高中",desc: "芬威克主教高中(Bishop Fenwick High School)位于马萨诸塞州的皮博迪，这座小城曾在2009年福布斯杂志评..."}
    ],
    idx: 0,
    pageNum: 0,
    windowHeight: "0px"
  },
  onLoad: function(){
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          windowHeight: res.windowHeight + "px"
        });
      }
    })
  },
  pullToRefresh: function(e){
    if(this.data.pageNum == 0){
      wx.showToast({
        title: '加载中...',
        icon: 'loading',
        duration: 1000,
        mask: true
      });
      this.setData({
        pageNum: 1,
        serverData: [
          {title: "手把手教你选择未来职业道路",desc: "你如何看待这个世界？将来要走哪条路？医学？法律？经济还是教育？这些看似大学生才会纠结的问题..."},
          {title: "【CIIE探校】第一站：苹果园学校",desc: "在走进位于马萨诸塞州中北部菲奇堡小城的苹果园学校之前，相信大家对它的印象大概只停留在以下..."},
          {title: "【CIIE探校】走进芬威克主教高中",desc: "芬威克主教高中(Bishop Fenwick High School)位于马萨诸塞州的皮博迪，这座小城曾在2009年福布斯杂志评..."},
          {title: "CIIE联合创始人周梅 | 做美高留学后端第一品牌",desc: "人们所熟知的，更多是提供留学前端服务的国内机构，而对产业链后端缺乏了解。事实上，美高留..."},
          {title: "我是如何逆袭考入美国排名第一大学的？",desc: "剑桥独家合作院校约翰保罗二世天主高中（PJPIITN）在读学生Kira获得美国服装设计大学排名第一的位于..."},
        ]
      });
    }else if(this.data.pageNum == 1){
      wx.showToast({
        title: '加载中...',
        icon: 'loading',
        duration: 1000,
        mask: true
      });
      this.setData({
        pageNum: 2,
        serverData: [
          {title: "手把手教你选择未来职业道路",desc: "你如何看待这个世界？将来要走哪条路？医学？法律？经济还是教育？这些看似大学生才会纠结的问题..."},
          {title: "【CIIE探校】第一站：苹果园学校",desc: "在走进位于马萨诸塞州中北部菲奇堡小城的苹果园学校之前，相信大家对它的印象大概只停留在以下..."},
          {title: "【CIIE探校】走进芬威克主教高中",desc: "芬威克主教高中(Bishop Fenwick High School)位于马萨诸塞州的皮博迪，这座小城曾在2009年福布斯杂志评..."},
          {title: "CIIE联合创始人周梅 | 做美高留学后端第一品牌",desc: "人们所熟知的，更多是提供留学前端服务的国内机构，而对产业链后端缺乏了解。事实上，美高留..."},
          {title: "我是如何逆袭考入美国排名第一大学的？",desc: "剑桥独家合作院校约翰保罗二世天主高中（PJPIITN）在读学生Kira获得美国服装设计大学排名第一的位于..."},
          {title: "吉尔莫学校正式进入施坦威项目",desc: "吉尔莫学校非常荣幸地宣布我们的学校已进入施坦威项目、这也意味着校园里的每一台钢琴将被置..."},
          {title: "凯斯西储大学参观",desc: "圣文森特圣玛丽高中（St. Vincent-St. Mary High School）定期会带领学生们参观大学。让在美国读..."}
        ]
      });
    }
    
  },
  gotoStudentReport: function(){
    wx.switchTab({
      url: '/pages/listOfReports/report'
    })
  },
  onShareAppMessage: function () {
    return {
      title: 'CIIE 学生报告自助查询',
      desc: 'CIIE学生报告自助查询',
      path: '/pages/home/index'
    }
  }
})
