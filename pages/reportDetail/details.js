//list.js
import API from '../../api/api';
import utils from '../../utils/util.js';
Page({
  data: {
    serverData: {},
    docs: [],
    tempTypeText: "",
    docURL: "",
    enumDescription: [
      "Student did not attend, refuses to speak, and / or has never spoken in class.", //0
      "Student participates but only gives one word answers.Student never participates in class discussions and rarely answers teacher’s questions", //1
      "Student will answer teacher’s questions with simplistic answers. Student will occasionally participate in class discussions, but participation is minimal", //2
      "Student answers teacher’s questions adequately. Student participates adequately in class discussions. Student demonstrates some critical thinking ability", //3
      "Student always fully answers teacher’s questions and shows critical thinking skills. During class discussions, student will ask classmates questions and participate fully.", //4
      "Student fully answers student questions and responds with critical thinking. During class discussions, student will occasionally act as discussion leader and encourages critical thinking in others" //5
    ],
    hasRead: "false",
    originalDocId: "",
    translatedDocId: "",
    hasReadOriginalDoc: "false",
    hasReadTranslatedDoc: "false",
    reportId: "",
    studentId: ""
  },
  translateText: function (content){
    var str = '';
    if(content == 'INPROGRESS'){
        str = 'In Progress';
    }else if(content){
        var strArr = content.split('_');
        for(var i = 0 ; i < strArr.length;i++){
            strArr[i] = (strArr[i].substr(0,1)).toUpperCase() + (strArr[i].substr(1)).toLowerCase()
        }
        str = strArr.join(' ');
        content = str;
    }else{
        content = '';
    }
    return str;
  },
  getImageArrays: function(docs){
    var retArrays = new Array();
    for(var i = 0;i< docs.length;i++){
        var index = docs[i].indexOf("?s=");
        var tempStr = docs[i].substr(0, index);
        var extensionName = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length).toLowerCase();

        if(extensionName == ".png" || extensionName == ".jpg" || extensionName == ".gif"|| extensionName == ".bmp"){
            retArrays.push(docs[i]);
        }
    }
    return retArrays;
  },
  onLoad: function (e) {
      if (e.hasReadTranslatedDoc != undefined){
        this.setData({ 
          originalDocId: e.originalDocId,
          translatedDocId: e.translatedDocId,
          // hasReadTranslatedDoc: e.hasReadTranslatedDoc, 
          // hasReadOriginalDoc: e.hasReadOriginalDoc, 
          reportId: e.reportId, 
          studentId: e.studentId
        });
      } else if (e.hasRead != undefined) {
        this.setData({ hasRead: e.hasRead, reportId: e.reportId, studentId: e.studentId });
      }
      if (e.type == "initial"){
          wx.setNavigationBarTitle({ title: '新生报告 - 详情' });
      } else if (e.type == "residential") {
          wx.setNavigationBarTitle({ title: '生活报告 - 详情' });
      } else if (e.type == "academic") {
          wx.setNavigationBarTitle({ title: '学校成绩 - 详情' });
      } else if (e.type == "esl") {
        wx.setNavigationBarTitle({ title: '剑桥学术服务 - 详情' });
      }
      var that = this; this.setData({ tempTypeText: e.type, docURL: ((e.docURL == "")?"":e.docURL + "?s=" + e.s)});
      wx.showLoading({
            title: '',
            icon: 'loading',
            mask:true
      });
      API.reportDetailsV2({
          method:'POST',
          reportId: e.reportId,
          type: e.type,
          studentId: e.studentId,
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId")
          },
          success:function(json){
              //Initial、Residential、Academic
              json.data = json.data.data;
              if (json.data.status == undefined){
                wx.showModal({
                  title: '提示',
                  content: "链接断开",
                  showCancel: false
                })
                wx.hideLoading();
                return;
              }
              if(json.data.status == "error"){
                wx.showModal({
                    title: '提示',
                    content: json.data.message,
                    showCancel: false
                })
              }else{
                if(that.data.tempTypeText == "academic"){
                    json.data.data.detail.type = that.translateText(json.data.data.detail.type);
                }
                if(that.data.tempTypeText == "residential" && json.data.data.detail.type == "FULL"){
                    if(json.data.data.detail != undefined){
                      json.data.data.detail.reportFor = json.data.data.detail.full.month;
                      json.data.data.detail.full.today = new Date(json.data.data.detail.full.today).Format("MM/dd/yyyy");
                      
                      if (json.data.data.detail.readOnly.submittedDate != undefined){
                        json.data.data.detail.readOnly.submittedDate = new Date(json.data.data.detail.readOnly.submittedDate).Format("MM/dd/yyyy");
                      }
                      if (json.data.data.detail.readOnly.translatedDate != undefined) {
                        json.data.data.detail.readOnly.translatedDate = new Date(json.data.data.detail.readOnly.translatedDate).Format("MM/dd/yyyy");
                      }
                    }
                } else if (that.data.tempTypeText == "residential" && json.data.data.detail.type == "CHECK_IN") {
                    if (json.data.data.detail != undefined) {
                      json.data.data.detail.reportFor = json.data.data.detail.checkIn.month;
                      json.data.data.detail.checkIn.today = new Date(json.data.data.detail.checkIn.today).Format("MM/dd/yyyy");

                      if (json.data.data.detail.readOnly.submittedDate != undefined) {
                        json.data.data.detail.readOnly.submittedDate = new Date(json.data.data.detail.readOnly.submittedDate).Format("MM/dd/yyyy");
                      }
                      if (json.data.data.detail.readOnly.translatedDate != undefined) {
                        json.data.data.detail.readOnly.translatedDate = new Date(json.data.data.detail.readOnly.translatedDate).Format("MM/dd/yyyy");
                      }
                    }
                }
                if (that.data.tempTypeText == "esl" && json.data.data.detail.type == "FULL") {
                  if (json.data.data.detail != undefined) {
                    json.data.data.detail.reportFor = json.data.data.detail.full.month;

                    if (json.data.data.detail.full.from != undefined) {
                      json.data.data.detail.full.from = new Date(json.data.data.detail.full.from).Format("MM-dd-yyyy");
                    }
                    if (json.data.data.detail.full.to != undefined) {
                      json.data.data.detail.full.to = new Date(json.data.data.detail.full.to).Format("MM-dd-yyyy");
                    }
                  }
                } else if (that.data.tempTypeText == "esl" && json.data.data.detail.type == "CHECK_IN") {
                  if (json.data.data.detail != undefined) {
                    json.data.data.detail.reportFor = json.data.data.detail.checkIn.month;

                    if (json.data.data.detail.checkIn.from != undefined) {
                      json.data.data.detail.checkIn.from = new Date(json.data.data.detail.checkIn.from).Format("MM-dd-yyyy");
                    }
                    if (json.data.data.detail.checkIn.to != undefined) {
                      json.data.data.detail.checkIn.to = new Date(json.data.data.detail.checkIn.to).Format("MM-dd-yyyy");
                    }
                    if (json.data.data.detail.checkIn.rankParticipation != undefined) {
                      json.data.data.detail.checkIn.enumDescription = " - " + that.data.enumDescription[json.data.data.detail.checkIn.rankParticipation];
                    }
                  }
                }
                json.data.data.detail.imageArrays = that.getImageArrays(json.data.data.docs);
                if (json.data.data.originalDocUrl != undefined){
                  json.data.data.detail.originalDocUrl = json.data.data.originalDocUrl;
                  json.data.data.detail.hasReadOriginalDoc = json.data.data.hasReadOriginalDoc.toString();
                }
                if (json.data.data.translatedDocUrl != undefined) {
                  json.data.data.detail.translatedDocUrl = json.data.data.translatedDocUrl;
                  json.data.data.detail.hasReadTranslatedDoc = json.data.data.hasReadTranslatedDoc.toString();
                }
                
                if (that.data.tempTypeText != "academic") {
                  json.data.data.detail.tempDocURL = that.data.docURL;
                  json.data.data.detail.hasRead = that.data.hasRead;
                  that.setData({
                    serverData: json.data.data.detail,
                    docs: json.data.data.docs,
                  });
                }else{
                  that.setData({
                    serverData: json.data.data.detail,
                    docs: json.data.data.docs,
                    hasReadTranslatedDoc: json.data.data.hasReadTranslatedDoc.toString(),
                    hasReadOriginalDoc: json.data.data.hasReadOriginalDoc.toString()
                  });
                }
              }
              
              wx.hideLoading();
          },
          fail:function(res){
              wx.showModal({
                title: '提示',
                content: '链接断开',
                showCancel: false
              });
          }
      });
  },
  getExtensionName: function (str) {
    var index = str.indexOf("?s=");
    var tempStr = str.substr(0, index);
    var returnStr = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length);
    return returnStr;
  },
  openTranslateDocument: function (e) {
    var that = this; var tempDataset = e.currentTarget.dataset;
    if (e.currentTarget.dataset.url == undefined || e.currentTarget.dataset.url.trim() == "") {
      var modalContent = (this.data.tempTypeText == 'academic') ? '暂无学校成绩报告。' : '暂无翻译报告。';
      if (tempDataset.docflag == "Translated"){
          modalContent = '暂无翻译报告。';
      }
      wx.showModal({
        title: '提示',
        content: modalContent,
        showCancel: false
      });
    } else {
      wx.getNetworkType({
        success: function (res) {
          // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
          var networkType = res.networkType;
          if (networkType == "none") {
            wx.showModal({
              title: '提示',
              content: '当前无网络，链接后重试！',
              showCancel: false
            });
            return;
          } else if (networkType == "wifi") {
            wx.showLoading({
              title: '下载中...',
              mask: true
            });

            var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
            wx.downloadFile({
              url: e.currentTarget.dataset.url + "&a=" + extensionName,
              success: function (res) {
                var filePath = res.tempFilePath;
                wx.showLoading({
                  title: '打开中...',
                  mask: true
                });
                if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                  that.setData({
                    imgSrc: filePath
                  });
                } else {
                  wx.openDocument({
                    filePath: filePath,
                    success: function (res) {
                      wx.hideLoading();
                      console.log('打开文档成功');

                      if (that.data.tempTypeText != "academic") {
                        if (that.data.hasRead == "false") {
                          that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, null, null);
                        }
                      } else {
                        if (tempDataset.docflag == "Original"){
                          if (that.data.hasReadOriginalDoc == "false") {
                            that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, that.data.originalDocId, 'Original');
                          }
                        } else if (tempDataset.docflag == "Translated"){
                          if (that.data.hasReadTranslatedDoc == "false") {
                            that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, that.data.translatedDocId, 'Translated');
                          }
                        }
                      }
                    },
                    fail: function (err) {
                      wx.hideLoading()
                      wx.showModal({
                        title: '提示',
                        content: err.errMsg,
                        showCancel: false
                      });
                    }
                  });
                }
              },
              fail: function (err) {
                wx.hideLoading()
                wx.showModal({
                  title: '提示',
                  content: '网络不好，请稍候重试。',
                  showCancel: false
                });
              }
            })
          } else if (networkType == "2g" || networkType == "3g" || networkType == "4g") {
            wx.showModal({
              title: '提示',
              content: '当前使用移动网络，请问要继续下载吗?',
              success: function (res) {
                if (res.confirm) {
                  wx.showLoading({
                    title: '下载中...',
                    mask: true
                  });
                  var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
                  wx.downloadFile({
                    url: e.currentTarget.dataset.url + "&a=" + extensionName,
                    success: function (res) {
                      var filePath = res.tempFilePath;
                      wx.showLoading({
                        title: '打开中...',
                        mask: true
                      });
                      if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                        that.setData({
                          imgSrc: filePath
                        });
                      } else {
                        wx.openDocument({
                          filePath: filePath,
                          success: function (res) {
                            wx.hideLoading()
                            console.log('打开文档成功');

                            if (that.data.tempTypeText != "academic") {
                              if (that.data.hasRead == "false") {
                                that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, null, null);
                              }
                            } else {
                              if (tempDataset.docflag == "Original") {
                                if (that.data.hasReadOriginalDoc == "false") {
                                  that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, that.data.originalDocId, 'Original');
                                }
                              } else if (tempDataset.docflag == "Translated") {
                                if (that.data.hasReadTranslatedDoc == "false") {
                                  that.flagReportRead(that.data.studentId, that.data.reportId, that.data.tempTypeText, that.data.translatedDocId, 'Translated');
                                }
                              }
                            }
                          },
                          fail: function (err) {
                            wx.hideLoading()
                            wx.showModal({
                              title: '提示',
                              content: err.errMsg,
                              showCancel: false
                            });
                          }
                        });
                      }
                    },
                    fail: function (err) {
                      wx.hideLoading()
                      wx.showModal({
                        title: '提示',
                        content: '网络不好，请稍候重试。',
                        showCancel: false
                      });
                    }
                  })
                }
              }
            });
          }
        }
      });
    }
  },
  flagReportRead: function (sId, reportId, reportType, docId, docFlag) {
    var that = this; var rId = reportId; var docFlag = docFlag;
    var paramObj = {
      studentId: sId,
      reportId: reportId,
      type: reportType
    }
    if (reportType == "academic" && docId != null) {
      paramObj.docId = docId;
    }else{
      paramObj.docId = "";
    }
    API.reportChangeToRead({
      method: 'POST',
      data: paramObj,
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      success: function (json) {
        if (json.data.data.status == 1) {
          if (paramObj.docId == ""){
            that.setData({ hasRead: "true" });
            that.data.serverData.hasRead = "true";
          }else{
            //设置 原始 和 翻译 报告，已读
            if (docFlag == "Original"){
              that.setData({ hasReadOriginalDoc: "true" });
              that.data.serverData.hasReadOriginalDoc = "true";
            } else if (docFlag == "Translated") {
              that.setData({ hasReadTranslatedDoc: "true" });
              that.data.serverData.hasReadTranslatedDoc = "true";
            }
          }
          
          that.setData({ serverData: that.data.serverData });
          console.log("标记成功");
        } else {
          console.log("标记失败");
        }
      },
      fail: function (err) {
        console.log("标记失败");
      }
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
