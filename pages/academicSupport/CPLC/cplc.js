//index.js
import API from '../../../api/api';
import utils from '../../../utils/util.js';
var app = getApp()
Page({
  data: {
    viewHeight: "0px",
    marginTop: "0px",
    updateNum: []
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.platform == "ios") {
          that.setData({
            viewHeight: (res.windowHeight) + "px",
            marginTop: "0px"
          });
        } else {
          that.setData({
            viewHeight: (res.windowHeight) + "px",
            marginTop: "-6px"
          });
        }
      }
    });
    
    var times = new Date(); var that = this;
    var subTimes = new Date(times.getTime() - 1000 * 60 * 60 * 24 * 13);
    var startTime = subTimes.getFullYear() + "-" + (((subTimes.getMonth() + 1) < 10) ? "0" + (subTimes.getMonth() + 1) : (subTimes.getMonth() + 1)) + "-" + ((subTimes.getDate() < 10) ? "0" + subTimes.getDate() : subTimes.getDate()) + " 00:00:00";
    var endTime = times.getFullYear() + "-" + (((times.getMonth() + 1) < 10) ? "0" + (times.getMonth() + 1) : (times.getMonth() + 1)) + "-" + ((times.getDate() < 10) ? "0" + times.getDate() : times.getDate()) + " 23:59:59";

    API.recentUpdateArticlesByType({
      method: 'POST',
      data: [20, 21, 22, 23],
      startTime: startTime,
      endTime: endTime,
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      success: function (json) {
        if (json.data.code == 1) {
          for (var item in json.data.data) {
            json.data.data[item] = Number.MaxNum(json.data.data[item])
          }
          that.setData({
            updateNum: json.data.data
          });
        }
      }
    });
  },
  openList: function (e) {
    wx.navigateTo({
      url: '/pages/articleList/articleList?type=' + e.currentTarget.dataset.type + "&title=" + e.currentTarget.dataset.title
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
