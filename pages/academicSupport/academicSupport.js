//index.js
import API from '../../api/api';
import utils from '../../utils/util.js';
var app = getApp()
Page({
  data: {
    viewHeight: "0px",
    viewWidth: "226px",
    marginTop: "0px",
    updateNum: []
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.platform == "ios") {
          that.setData({
            viewHeight: (res.windowHeight) + "px",
            viewWidth: (res.windowWidth - 126) + "px",
            marginTop: "0px"
          });
        } else {
          that.setData({
            viewHeight: (res.windowHeight) + "px",
            viewWidth: (res.windowWidth - 126) + "px",
            marginTop: "-6px"
          });
        }
      }
    });

    var times = new Date(); var that = this;
    var subTimes = new Date(times.getTime() - 1000 * 60 * 60 * 24 * 13);
    var startTime = subTimes.getFullYear() + "-" + (((subTimes.getMonth() + 1) < 10) ? "0" + (subTimes.getMonth() + 1) : (subTimes.getMonth() + 1)) + "-" + ((subTimes.getDate() < 10) ? "0" + subTimes.getDate() : subTimes.getDate()) + " 00:00:00";
    var endTime = times.getFullYear() + "-" + (((times.getMonth() + 1) < 10) ? "0" + (times.getMonth() + 1) : (times.getMonth() + 1)) + "-" + ((times.getDate() < 10) ? "0" + times.getDate() : times.getDate()) + " 23:59:59";

    API.recentUpdateArticlesByType({
      method: 'POST',
      data: [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
      startTime: startTime,
      endTime: endTime,
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      success: function (json) {
        if (json.data.code == 1) {
          //16, 17, 18, 19
          //20, 21, 22, 23
          //24, 25, 26, 27, 28, 29
          var course1 = 0; var course2 = 0; var course3 = 0;
          for (var item in json.data.data){
            var index = parseInt(item);
            if (index >= 16 && index <= 19){
              course1 += json.data.data[item];
            }
            if (index >= 20 && index <= 23) {
              course2 += json.data.data[item];
            }
            if (index >= 24 && index <= 29) {
              course3 += json.data.data[item];
            }
          }
          var updateObjs = {};
          if (course1 != 0) { updateObjs[1] = Number.MaxNum(course1);}
          if (course2 != 0) { updateObjs[2] = Number.MaxNum(course2);}
          if (course3 != 0) { updateObjs[3] = Number.MaxNum(course3);}
          that.setData({
            updateNum: updateObjs
          });
        }
      }
    });
  },
  openList: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
