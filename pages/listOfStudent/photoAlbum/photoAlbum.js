import API from '../../../api/api';
Page({
  data: {
    sId: '',
    originalData: [],
    serverData: [],
    selectVals: [],
    currentPage: 1,
    totalPage: 0,
    pageSize: 0,
    isLoading: false,
    isNetOff: false,
    imgWidth: '0px',
    scrollHeight: "0px",
    previewHeight: "0px"
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          imgWidth: Math.floor((res.windowWidth - 24) / 3) + "px",
          scrollHeight: (res.windowHeight - 53) + "px"
        });
      }
    })
    that.setData({ sId: options.sid}, function(){
      that.onRefreshData(that.data.sId, 1, 20);
    });
  },
  onPullDownRefresh: function(){
    wx.stopPullDownRefresh();
    this.onRefreshData(this.data.sId, 1, (this.data.currentPage * 20));
  },
  bindRefreshData: function(){
    this.onRefreshData(this.data.sId, 1, 20);
  },
  bindScrollToLower: function(){
    if (this.data.currentPage < this.data.totalPage) {
      this.data.currentPage++;
      this.onRefreshData(this.data.sId, 1, (this.data.currentPage * 20));
    }
  },
  onRefreshData: function (sId, pageNumber, pageSize){
    var that = this;
    if (pageSize <= 20) {
      that.setData({ isLoading: true, isNetOff: false, pageSize: pageSize });
    } else {
      that.setData({ pageSize: pageSize });
    }
    API.studentPhotoAlbums({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      sId: sId,
      data: {
        conditions: {},
        pageNumber: pageNumber,
        pageSize: pageSize,
        dir: 'DESC'
      },
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isNetOff: true, serverData: [], totalPage: 0, currentPage: that.data.currentPage });
          return;
        }
        if (json.data.data == undefined) {
          wx.showModal({
            title: '提示',
            content: '抱歉，服务器连接错误，请联系管理员。',
            showCancel: false
          });
          that.setData({ isLoading: false, isNetOff: true });
          return;
        }
        if (json.data.code == 1 && json.data.data.status == "success"){
          var tempArray = [];
          for (var i = 0; i < json.data.data.data.content.length; i++){
            var obj = json.data.data.data.content[i];
            if(tempArray.length <= 0){
                var objItem = {
                  date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                  items: new Array(obj)
                };
                tempArray.push(objItem);
            }else{
                var haveData = false;
                for (var j = 0; j < tempArray.length; j++){
                  if (new Date(obj.dateCreated).Format("MM-dd-yyyy") == tempArray[j].date){
                    tempArray[j].items.push(obj);
                    haveData = true; break;
                  }
                }
                if (haveData == false){
                    var objItem = {
                      date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                      items: new Array(obj)
                    };
                    tempArray.push(objItem);
                }
            }
          }
          if (pageSize <= 20) {
            that.setData({ serverData: tempArray, totalPage: json.data.data.data.totalPage, originalData: json.data.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage});
          }else{
            that.setData({ serverData: tempArray, originalData: json.data.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage });
          }
          console.log(tempArray);
        }
      },
      fail: function (err) {
        if (that.data.currentPage >= 2) { that.data.currentPage--; }
        that.setData({ isLoading: false, isNetOff: true, currentPage: that.data.currentPage });
      }
    })
  },
  showDescription: function(e){
    if (e.target.dataset.val != ""){
      wx.showModal({
        content: e.target.dataset.val,
        showCancel: false
      });
    }
  },
  openImage: function(e){
    if (e.target.dataset.img != "/images/imgError.png"){
      wx.navigateTo({
        url: '/pages/listOfStudent/photoAlbum/previewImage/previewImage?img=' + encodeURIComponent(e.target.dataset.img) + "&desc=" + encodeURIComponent(e.target.dataset.description) + "&date=" + encodeURIComponent(e.target.dataset.date) + "&shareDate=" + encodeURIComponent(e.target.dataset.sharetoparenttime) + "&sId=" + this.data.sId + "&nDId=" + e.target.dataset.ndid + "&whetherSee=" + e.target.dataset.whethersee
      });
    }
  }
})
