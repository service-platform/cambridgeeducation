import API from '../../../../api/api';

Page({
  data: {
    sId: "",
    nDId: "",
    whetherSee: false,
    previewHeight: "0px",
    previewImage: "",
    description: "",
    date: "",
    shareDate: "",
    userName: ""
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          previewHeight: (res.windowHeight) + "px"
        });
      }
    });
    this.setData({
      sId: options.sId,
      nDId: options.nDId,
      whetherSee: (options.whetherSee == "true")?true:false,
      previewImage: decodeURIComponent(options.img),
      description: decodeURIComponent(options.desc),
      date: new Date(decodeURIComponent(options.date)).Format("yyyy-MM-dd hh:mm:ss"),
      shareDate: new Date(decodeURIComponent(options.shareDate)).Format("yyyy-MM-dd hh:mm:ss")
    }, function(){
        API.previewStudentAlbum({
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId")
          },
          ndId: that.data.nDId,
          success: function (json) {
            if (json.data.code == 1){
              console.log("标记已读：成功");
              that.setData({
                userName: json.data.data.data.userProfile.baseInfo.fullName
              });

              var pages = getCurrentPages();
              var prevPage = pages[pages.length - 2];  //上一个页面
              for (var i = 0; i < prevPage.data.originalData.length; i++){
                if (prevPage.data.originalData[i].id == that.data.nDId){
                  prevPage.data.originalData[i].whetherSee = true;
                  break;
                }
              }
              prevPage.setData({
                  originalData: prevPage.data.originalData
              },function(){
                var tempArray = [];
                for (var i = 0; i < prevPage.data.originalData.length; i++) {
                    var obj = prevPage.data.originalData[i];
                    if (tempArray.length <= 0) {
                      var objItem = {
                        date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                        items: new Array(obj)
                      };
                      tempArray.push(objItem);
                    } else {
                      var haveData = false;
                      for (var j = 0; j < tempArray.length; j++) {
                        if (new Date(obj.dateCreated).Format("MM-dd-yyyy") == tempArray[j].date) {
                          tempArray[j].items.push(obj);
                          haveData = true; break;
                        }
                      }
                      if (haveData == false) {
                        var objItem = {
                          date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                          items: new Array(obj)
                        };
                        tempArray.push(objItem);
                      }
                    }
                  }
                  prevPage.setData({ serverData: tempArray });
              });
            }
          },
          fail: function (err) {
            console.log("标记已读：失败");
            that.setData({ userName: "加载失败" });
          }
        });
    });
  }
})
