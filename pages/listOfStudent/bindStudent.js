//list.js
import API from '../../api/api';
Page({
  data: {
    servicesData: [],
    index: 0,
    isLoading: true,
    isError: false,
    viewWidth: '0px',
    isFirst: true
  },
  openDetails: function(e){
    wx.navigateTo({
      url: '/pages/listOfStudent/listOfReports/report?studentId=' + e.currentTarget.dataset.sid + "&stuName=" + encodeURIComponent(e.currentTarget.dataset.stuname)
    });
  },
  onPullDownRefresh: function () {
    var that = this;
    this.setData({ isFirst: true}, function(){
      that.refreshData();
      wx.stopPullDownRefresh();
    });
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({ viewWidth: (res.windowWidth - 160) + "px" });
      }
    })
  },
  onShow: function () {
    this.refreshData();
  },
  //学生列表
  refreshData:function(){
    var that = this;
    this.setData({ servicesData: (this.data.isFirst) ? [] : this.data.servicesData, isLoading: (this.data.isFirst) ? true : false, isError: false });
    API.listStudent({
        method: 'POST',
        data : {
            cdkey: wx.getStorageSync("openId")
        },
        header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId")
        },
        success:function(data){
            if(data.statusCode == 200){
                if(data.data.data.students == undefined){
                    that.setData({
                        servicesData: [],
                        isLoading: false,
                        isError: false,
                        isFirst: false
                    });
                }else{
                    that.setData({
                        servicesData: data.data.data.students,
                        isLoading: false,
                        isError: false,
                        isFirst: false
                    });
                }
            }else{
              that.setData({
                servicesData: [],
                isLoading: false,
                isError: true,
                isFirst: false
              });
            }
        },
        fail: function (err) {
          that.setData({ isLoading: false, isError: true, isFirst: false});
        }
    });
  },
  gotoPhotoAlbum: function(e){
      wx.navigateTo({
        url: '/pages/listOfStudent/photoAlbum/photoAlbum?sid=' + e.currentTarget.dataset.sid
      });
  },
  //添加绑定
  addBindStudent: function(){
      wx.navigateTo({
        url: '/pages/binding/binding?flag=false'
      });
  },
  //解绑学生
  gotoUnbindList: function(){
      wx.navigateTo({
        url: '/pages/listOfStudent/unBindStudent/unBindStudent'
      });
  },
  makePhoneCall: function () {
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  }
})
