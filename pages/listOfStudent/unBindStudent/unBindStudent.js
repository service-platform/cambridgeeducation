import API from '../../../api/api';
Page({
  data: {
    servicesData: [],
    isLoading: true,
    isError: false,
    disable: true,
    id: '',
    firstName: ''
  },
  onLoad: function (options) {
    this.refreshData();
  },
  onPullDownRefresh: function () {
    this.refreshData();
    wx.stopPullDownRefresh();
  },
  //学生列表
  refreshData: function () {
    var that = this;
    this.setData({ servicesData: [], isLoading: true, isError: false, disable: true });
    API.listStudent({
      method: 'POST',
      data: {
        cdkey: wx.getStorageSync("openId")
      },
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      success: function (data) {
        if (data.statusCode == 200) {
          if (data.data.data.students == undefined) {
            that.setData({
              servicesData: [],
              isLoading: false,
              isError: false
            });
          } else {
            that.setData({
              servicesData: data.data.data.students,
              isLoading: false,
              isError: false
            });
          }
        } else {
          that.setData({
            servicesData: [],
            isLoading: false,
            isError: true
          });
        }
      },
      fail: function (err) {
        that.setData({ isLoading: false, isError: true });
      }
    });
  },
  radioChange: function(e){
    this.setData({
      id: e.detail.value.split('||')[0],
      firstName: e.detail.value.split('||')[1],
      disable: false
    });
  },
  //解绑学生
  unBindStudent: function (e) {
    console.log("解绑：" + e.detail.formId);
    var page = this;
    wx.showModal({
      title: '提示',
      content: "你确定要解绑：" + page.data.firstName + " 吗？",
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '解绑中...',
            mask: true
          });

          API.unBindStudentJZH({
            method: 'POST',
            data: {
              sId: page.data.id,
              formId: e.detail.formId
            },
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("openId"),
            },
            success: function (data) {
              if (data.data.code == 1 && (data.data.data.status == "success" || data.data.data.message == "该学生已经解绑")) {
                wx.hideLoading();
                wx.showModal({
                  title: '提示',
                  content: "解绑成功。",
                  showCancel: false,
                });
                page.refreshData();
              } else {
                wx.hideLoading();
                wx.showModal({
                  title: '提示',
                  content: "解绑失败。",
                  showCancel: false,
                });
              }
            },
            fail: function (err) {
              wx.hideLoading();
              wx.showModal({
                title: '提示',
                content: "解绑失败。",
                showCancel: false,
              });
            }
          });
        }
      }
    });
  },
  makePhoneCall: function () {
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  }
})