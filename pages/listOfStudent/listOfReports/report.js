//list.js
import API from '../../../api/api';
import utils from '../../../utils/util.js';
Page({
  data: {
    isFirst: true,
    initialListData: [{
      key: "新生报告 Initial Arrival Report",
      items: []
    }],
    residentialListData: [{
      key: "生活报告 Residential Progress Report",
      items: []
    }],
    academicListData: [{
      key: "学校成绩 School Academic Report",
      items: []
    }],
    eslListData: [{
      key: "剑桥学术服务 Cambridge Academic Service Report",
      items: []
    }],
    initialFlag: true,
    residentialFlag: false,
    academicFlag: false,
    eslFlag: false,
    imgSrc: '',
    studentId: '',
    viewHeight: '0px',
    marginTop: '0px',
    viewDisplay: 'none',
    txtView: '下载中...',
    downloadMarginTop: '28px'
  },
  openDetails: function(e){
    if (e.currentTarget.dataset.hasread != undefined){
      wx.navigateTo({
        url: '/pages/reportDetail/details?reportId=' + e.currentTarget.dataset.reportid + "&type=" + e.currentTarget.dataset.type + "&studentId=" + this.data.studentId + "&docURL=" + ((e.currentTarget.dataset.docurl == undefined) ? "" : e.currentTarget.dataset.docurl.replace("?s=", "&s=")) + "&hasRead=" + e.currentTarget.dataset.hasread
      });
    } else if (e.currentTarget.dataset.hasreadoriginaldoc != undefined) {
      wx.navigateTo({
        url: '/pages/reportDetail/details?reportId=' + e.currentTarget.dataset.reportid + "&type=" + e.currentTarget.dataset.type + "&studentId=" + this.data.studentId + "&docURL=" + ((e.currentTarget.dataset.docurl == undefined) ? "" : e.currentTarget.dataset.docurl.replace("?s=", "&s=")) + "&hasReadOriginalDoc=" + e.currentTarget.dataset.hasreadoriginaldoc + "&hasReadTranslatedDoc=" + e.currentTarget.dataset.hasreadtranslateddoc + "&originalDocId=" + e.currentTarget.dataset.originaldocid + "&translatedDocId=" + e.currentTarget.dataset.translateddocid
      });
    }
    wx.setStorageSync("reportType", e.currentTarget.dataset.type);
  },
  onLoad: function (param) {
    this.data.studentId = param.studentId;
    this.firstRefreshData("initial");
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.platform == "android"){
          that.setData({ viewHeight: res.windowHeight + "px", marginTop: ((res.windowHeight / 2) - 60) + 'px', downloadMarginTop: 'inherit'});
        } else if (res.platform == "ios") {
          that.setData({ viewHeight: res.windowHeight + "px", marginTop: ((res.windowHeight / 2) - 60) + 'px', downloadMarginTop: '-4px' });
        } else{
          that.setData({ viewHeight: res.windowHeight + "px", marginTop: ((res.windowHeight / 2) - 60) + 'px', downloadMarginTop: 'inherit' });
        }
      }
    });

    if (getCurrentPages()[getCurrentPages().length - 1].route == "pages/listOfStudent/listOfReports/report") {
      wx.setNavigationBarTitle({ title: decodeURIComponent(param.stuName) + ': 学生报告' });
    }
  },
  onShow: function(){
    if(this.data.isFirst == false){
      this.refreshData(wx.getStorageSync("reportType"), true);
    }else{
      this.setData({ isFirst: false });
    }
  },
  selectGroupItem: function(e){
    if (e.currentTarget.dataset.flag == "initial"){
        if(this.data.initialFlag == true){
            this.setData({
                initialFlag: false,
            });
        }else{
            this.refreshData(e.currentTarget.dataset.flag, false);
        }
    }else if (e.currentTarget.dataset.flag == "residential"){
        if(this.data.residentialFlag == true){
            this.setData({
                residentialFlag: false,
            });
        }else{
            this.refreshData(e.currentTarget.dataset.flag, false);
        }
    }else if (e.currentTarget.dataset.flag == "academic"){
        if(this.data.academicFlag == true){
            this.setData({
                academicFlag: false,
            });
        }else{
            this.refreshData(e.currentTarget.dataset.flag, false);
        }
    }else if (e.currentTarget.dataset.flag == "esl") {
      if (this.data.eslFlag == true) {
        this.setData({
          eslFlag: false,
        });
      } else {
        this.refreshData(e.currentTarget.dataset.flag, false);
      }
    }
  },
  //initial, residential, academic, esl
  firstRefreshData:function(tapType){
    var tempTapType = tapType; var that = this;
    wx.showLoading({ title: '加载中...', mask: true });

    var keyTitle = "新生报告 Initial Arrival Report";
    if(tapType == "residential"){
        keyTitle = "生活报告 Residential Progress Report";
    }else if(tapType == "academic"){
        keyTitle = "学校成绩 School Academic Report";
    }else if(tapType == "esl"){
        keyTitle = "剑桥学术服务 Cambridge Academic Service Report";
    }
    wx.setStorageSync("reportType", tapType);
    API.viewReportV2({
        method: 'POST',
        studentId: this.data.studentId,
        type: tempTapType,
        header:{
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId")
        },
        success:function(json){
            json.data = json.data.data;
            if (json.data == undefined) { 
              wx.showModal({
                title: '提示',
                content: "抱歉，服务器连接错误，请联系管理员。",
                showCancel: false,
              });
              wx.hideLoading(); return;
            }
            if(json.data.status == "success"){    
                if(tempTapType == "initial"){
                    that.setData({
                        initialListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        initialFlag: true
                    });
                }else if(tempTapType == "residential"){
                    that.setData({
                        residentialListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        residentialFlag: true
                    });
                }else if(tempTapType == "academic"){
                    that.setData({
                        academicListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        academicFlag: true
                    });
                }else if(tempTapType == "esl"){
                    that.setData({
                        eslListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        eslFlag: true
                    });
                }
            } else if (json.data.status == "fail") {
              wx.showModal({
                title: '提示',
                content: json.data.message,
                showCancel: false
              });
            } else if (json.data.status == "error") {
              wx.showModal({
                title: '提示',
                content: json.data.message,
                showCancel: false,
              });
            }
            wx.hideLoading();
        },
        fail:function(){
            wx.hideLoading();
            wx.showModal({
              title: '提示',
              content: '链接断开',
              showCancel: false,
            });
        }
    });
  },
  iconClose: function(){
    this.setData({imgSrc: ''});
  },
  getExtensionName: function(str){
    var index = str.indexOf("?s=");
    var tempStr = str.substr(0, index);
    var returnStr = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length);
    return returnStr;
  },
  openTranslateDocument: function(e){
    var that = this; var tempDataset = e.currentTarget.dataset;
    if (e.currentTarget.dataset.url == undefined || e.currentTarget.dataset.url.trim() == ""){
        wx.showModal({
            title: '提示',
            content: '暂无翻译报告。',
            showCancel: false
        });
    }else{
      wx.getNetworkType({
        success: function (res) {
          // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
          var networkType = res.networkType;
          if (networkType == "none"){
              wx.showModal({
                title: '提示',
                content: '当前无网络，链接后重试！',
                showCancel: false
              });
              return;
          } else if (networkType == "wifi"){
              that.setData({
                viewDisplay: 'block',
                txtView: '下载中...'
              });
              
              var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
              console.log(e.currentTarget.dataset.url + "&a=" + extensionName);
              wx.downloadFile({
                url: e.currentTarget.dataset.url + "&a=" + extensionName,
                success: function (res) {
                  var filePath = res.tempFilePath;
                  that.setData({
                    viewDisplay: 'block',
                    txtView: '打开中...'
                  });
                  if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                    that.setData({
                      imgSrc: filePath
                    });
                  } else {
                    wx.openDocument({
                      filePath: filePath,
                      success: function (res) {
                        that.setData({ viewDisplay: 'none' });
                        console.log('打开文档成功');
                        if (tempDataset.hasread != undefined){
                          if (tempDataset.hasread == false) {
                            that.flagReportRead(that.data.studentId, tempDataset.reportid, tempDataset.reporttype, null, null);
                          }
                        } else if (tempDataset.hasreadtranslateddoc != undefined) {
                          if (tempDataset.hasreadtranslateddoc == false) {
                            that.flagReportRead(that.data.studentId, tempDataset.reportid, tempDataset.reporttype, tempDataset.docid, 'Translated');
                          }
                        }
                      },
                      fail: function (err) {
                        that.setData({ viewDisplay: 'none' });
                        wx.showModal({
                          title: '提示',
                          content: err.errMsg,
                          showCancel: false
                        });
                      }
                    });
                  }
                },
                fail: function (err) {
                  that.setData({ viewDisplay: 'none' });
                  wx.showModal({
                    title: '提示',
                    content: '网络不好，请稍候重试。',
                    showCancel: false
                  });
                }
              })
          } else if (networkType == "2g" || networkType == "3g" || networkType == "4g") {
              wx.showModal({
                title: '提示',
                content: '当前使用移动网络，请问要继续下载吗?',
                success: function (res) {
                  if (res.confirm) {
                      that.setData({
                        viewDisplay: 'block',
                        txtView: '下载中...'
                      });

                      var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
                      wx.downloadFile({
                        url: e.currentTarget.dataset.url + "&a=" + extensionName,
                        success: function (res) {
                          var filePath = res.tempFilePath;
                          that.setData({
                            viewDisplay: 'block',
                            txtView: '打开中...'
                          });
                          if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                            that.setData({
                              imgSrc: filePath
                            });
                          } else {
                            wx.openDocument({
                              filePath: filePath,
                              success: function (res) {
                                that.setData({ viewDisplay: 'none' });
                                console.log('打开文档成功');
                                if (tempDataset.hasread != undefined) {
                                  if (tempDataset.hasread == false) {
                                    that.flagReportRead(that.data.studentId, tempDataset.reportid, tempDataset.reporttype, null, null);
                                  }
                                } else if (tempDataset.hasreadtranslateddoc != undefined) {
                                  if (tempDataset.hasreadtranslateddoc == false) {
                                    that.flagReportRead(that.data.studentId, tempDataset.reportid, tempDataset.reporttype, tempDataset.docid, 'Translated');
                                  }
                                }
                              },
                              fail: function (err) {
                                that.setData({ viewDisplay: 'none' });
                                wx.showModal({
                                  title: '提示',
                                  content: err.errMsg,
                                  showCancel: false
                                });
                              }
                            });
                          }
                        },
                        fail: function (err) {
                          that.setData({ viewDisplay: 'none' });
                          wx.showModal({
                            title: '提示',
                            content: '网络不好，请稍候重试。',
                            showCancel: false
                          });
                        }
                      })
                  }
                }
              });
          }
        }
      });
    }
  },
  flagReportRead: function (sId, reportId, reportType, docId, docFlag){
    var that = this; var rId = reportId; var docFlag = docFlag;
    var paramObj = {
      studentId: sId,
      reportId: reportId,
      type: reportType
    }
    if (reportType == "academic" && docId != null){
      paramObj.docId = docId;
    }else{
      paramObj.docId = "";
    }

    API.reportChangeToRead({
      method:'POST',
      data: paramObj,
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId")
      },
      success: function(json){
        if (json.data.data.status == 1){
          if (reportType == 'initial') {
            for (var i = 0; i < that.data.initialListData[0].items.length; i++) {
              if (that.data.initialListData[0].items[i].reportId == rId) {
                that.data.initialListData[0].items[i].hasRead = true;
                break;
              }
            }
            that.setData({ initialListData: that.data.initialListData });
          } else if (reportType == 'residential') {
            for (var i = 0; i < that.data.residentialListData[0].items.length; i++) {
              if (that.data.residentialListData[0].items[i].reportId == rId) {
                that.data.residentialListData[0].items[i].hasRead = true;
                break;
              }
            }
            that.setData({ residentialListData: that.data.residentialListData });
          } else if (reportType == 'academic') {
            for (var i = 0; i < that.data.academicListData[0].items.length; i++) {
              if (that.data.academicListData[0].items[i].reportId == rId) {
                if (docFlag == 'Translated') {
                  that.data.academicListData[0].items[i].hasReadTranslatedDoc = true;
                }
                break;
              }
            }
            that.setData({ academicListData: that.data.academicListData });
          } else if (reportType == 'esl') {
            for (var i = 0; i < that.data.eslListData[0].items.length; i++) {
              if (that.data.eslListData[0].items[i].reportId == rId) {
                that.data.eslListData[0].items[i].hasRead = true;
                break;
              }
            }
            that.setData({ eslListData: that.data.eslListData });
          }
          console.log("标记成功");
        }else{
          console.log("标记失败");
        }
      },
      fail: function(err){
        console.log("标记失败");
      }
    });
  },
  refreshData: function(tapType, flag){
    var tempTapType = tapType; var that = this;
    if (flag == false){
      wx.showLoading({ title: '加载中...', mask: true });
    }

    var keyTitle = "新生报告 Initial Arrival Report";
    if(tapType == "residential"){
        keyTitle = "生活报告 Residential Progress Report";
    }else if(tapType == "academic"){
        keyTitle = "学校成绩 School Academic Report";
    }else if(tapType == "esl"){
        keyTitle = "剑桥学术服务 Cambridge Academic Service Report";
    }
    wx.setStorageSync("reportType", tapType);
    API.viewReportV2({
        method: 'POST',
        studentId: this.data.studentId,
        type: tempTapType,
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId")
        },
        success :function(json){
            json.data = json.data.data;
            if (json.data == undefined) {
              wx.showModal({
                title: '提示',
                content: "抱歉，服务器连接错误，请联系管理员。",
                showCancel: false,
              });
              wx.hideLoading(); return;
            }
            if(json.data.status == "success"){
                if(tempTapType == "initial"){
                    that.setData({
                        initialListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        initialFlag: true
                    });
                }else if(tempTapType == "residential"){
                    that.setData({
                        residentialListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        residentialFlag: true
                    });
                }else if(tempTapType == "academic"){
                    that.setData({
                        academicListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        academicFlag: true
                    });
                }else if(tempTapType == "esl"){
                    that.setData({
                        eslListData: [{
                            key: keyTitle,
                            items: json.data.data
                        }],
                        eslFlag: true
                    });
                }
            } else if (json.data.status == "fail") {
                wx.showModal({
                  title: '提示',
                  content: json.data.message,
                  showCancel: false
                });
            } else if (json.data.status == "error") {
                wx.showModal({
                  title: '提示',
                  content: json.data.message,
                  showCancel: false,
                });
            }
            wx.hideLoading();
        },
        fail: function(err){
            wx.hideLoading();
            wx.showModal({
              title: '提示',
              content: '链接断开',
              showCancel: false
            });
        }
    });
  },
  makePhoneCall: function () {
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
