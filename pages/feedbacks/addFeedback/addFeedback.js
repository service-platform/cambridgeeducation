import API from '../../../api/api';

Page({
  data:{
    disabled: false,
    imgSource: '/image/addImage.png',
    imgNumber: 0,
    txtNumber: 0,
    platform: "",
    txtWidth: "0px",
    containerHeight: "0px"
  },
  //解决 IOS 系统，提交无反应
  hideKeyboard: function () {
    wx.hideKeyboard();
  },
  onLoad: function (param) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          txtWidth: (res.windowWidth - ((res.platform=='ios')?10:20)) + "px",
          btnViewWidth: (res.windowWidth - 20) + "px",
          containerHeight: (res.windowHeight) + "px",
          platform: res.platform
        });
      }
    })
  },
  txtInputInfo: function(e){
    this.setData({txtNumber: e.detail.value.length});
  },
  btnChooseImage: function(){
    var that = this;
    wx.chooseImage({
      count: 1, // 选择 1 张
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths 
        that.setData({
          imgSource: res.tempFilePaths[0],
          imgNumber: 1
        });
      }
    })
  },
  btnBack: function(){
    wx.navigateBack();
  },
  bindFormSubmit:function(e){
    var that = this;
    if(e.detail.value.feedbackContent == ""){
        wx.showModal({
          title: '提示:',
          content: '反馈信息，不能为空。',
          showCancel: false
        });
        return;
    }
    if (e.detail.value.feedbackContent.length <= 5) {
        wx.showModal({
          title: '提示:',
          content: '反馈信息，不能小于 5 个字。',
          showCancel: false
        });
        return;
    }
    that.setData({disabled: true});

    if(this.data.imgSource == '/image/addImage.png'){
        API.addFeedback({
            method: 'POST',
            header: {
                'Content-Type': 'application/json',
                userAuth: wx.getStorageSync("openId"),
            },
            data: {
              feedbackContent: e.detail.value.feedbackContent,
              status: '0'
            },
            success:function(data){
                if(data.data.code == 1){
                    wx.showModal({
                      title: '提示:',
                      content: '反馈信息成功。',
                      showCancel: false,
                      success: function(res) {
                        if (res.confirm) {
                          that.setData({disabled: false});
                          wx.navigateBack();
                        }
                      }
                    });
                }
            },
            fail: function(err){
                wx.showModal({
                    title: '提示:',
                    content: '反馈提交失败。',
                    showCancel: false
                });
                that.setData({disabled: false});
            }
        });
    }else{
        wx.uploadFile({
            url: API.host_backend + '/webAPI/Feedback/feedbackWithPic',
            filePath: this.data.imgSource,
            name: 'fbPhoto',
            header: {
                'Content-Type': 'multipart/form-data',
                userAuth: wx.getStorageSync("openId")
            },
            formData:{
                feedbackContent: e.detail.value.feedbackContent,
                status: '0'
            },
            success: function(data){
                var tempObj = JSON.parse(data.data);
                if(tempObj.code == 1){
                    wx.showModal({
                      title: '提示:',
                      content: '反馈信息成功。',
                      showCancel: false,
                      success: function(res) {
                        if (res.confirm) {
                          that.setData({disabled: false});
                          wx.navigateBack();
                        }
                      }
                    });
                }
            },
            fail: function(err){
                wx.showModal({
                    title: '提示:',
                    content: '反馈提交失败。',
                    showCancel: false
                });
                that.setData({disabled: false});
            }
        })
    }
  }
})