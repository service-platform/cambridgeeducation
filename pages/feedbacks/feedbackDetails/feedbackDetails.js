import API from '../../../api/api';
Page({
  data:{
    serverData: {},
    selectedSrc: '/image/articleDetails/selected.png'
  },
  openImages:function(e){
    wx.previewImage({
      current: e.target.dataset.pic, // 当前显示图片的http链接
      urls: [e.target.dataset.pic] // 需要预览的图片http链接列表
    })
  },
  onLoad:function(option){
    var that = this;
    API.feedbackDetail({
      id: option.id,
      header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
      },
      success :function(data){
          if(data.data.code == 1){
            var feedback = data.data.data.feedback;
            if(feedback.replyTime != undefined){
                feedback.replyTime = new Date(feedback.replyTime).Format('yyyy-MM-dd hh:mm:ss');
            }
            var feedback = data.data.data.feedback;
            if(feedback.feedbackTime != undefined){
                feedback.feedbackTime = new Date(feedback.feedbackTime).Format('yyyy-MM-dd hh:mm:ss');
            }
            if (feedback.score != undefined){feedback.score = parseInt(feedback.score);}
            that.setData({
              serverData: data.data.data
            });
          }
      }
    })
  }
})