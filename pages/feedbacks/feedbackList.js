//list.js
import API from '../../api/api';
Page({
  data: {
    servicesData: [],
    index: 0,
    //评分功能
    stars: [0, 1, 2, 3, 4],
    normalSrc: '/image/articleDetails/normal.png',
    selectedSrc: '/image/articleDetails/selected.png',
    key: 0,
    fbid: 0,
    showScore: false,
    marginTop: "0px",
    scrollHeight: "0px",
    maskedHeight: "0px",
    pageSize: 10,
    totalPage: 0,
    platform: "",
    isLoading: true,
    isError: false,
    adaptiveWidth: "0px",
    leftScore: "0px",
    isFirst: true,
    currentPage: 1
  },
  onLoad: function (option) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.platform == "android" || res.platform == "devtools") {
          that.setData({ platform: res.platform});
        } else {
          that.setData({ platform: res.platform});
        }
        that.setData({
          marginTop: (parseInt(res.windowHeight / 2) - 46) + "px",
          scrollHeight: (res.windowHeight - 32) + "px",
          maskedHeight: res.windowHeight + "px",
          adaptiveWidth: (res.windowWidth - 89) + "px",
          leftScore: parseInt((res.windowWidth - parseInt(res.windowWidth * 0.86)) / 2) + "px"
        });
      }
    });
    
    this.setData({ isLoading: true, isError: false });
    this.refreshData();
  },
  onShow: function(){
    this.refreshData();
  },
  selectStar: function (e) {
    var key = e.currentTarget.dataset.key
    this.setData({key: key});
  },
  closeScoreView: function(){
    this.setData({ showScore: false });
  },
  onPullDownRefresh: function () {
    this.refreshData();
    wx.stopPullDownRefresh();
  },
  bindScrollToLower: function () {
    if ((this.data.totalPage > this.data.currentPage) ) {
      this.data.pageSize += 10;
      this.setData({ currentPage: ++this.data.currentPage });
      this.refreshData();
    }
  },
  btnSubmitScore: function(){
    if (this.data.key <= 0){
      wx.showModal({
        title: '提示：',
        content: '评分不能为零',
        showCancel: false
      });
      return;
    }
    var that = this;
    API.scoreByFeedback({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      fbId: this.data.fbid,
      score: this.data.key,
      success: function (data) {
        if (data.data.code == 1) {
          wx.showToast({
              title: '已评分！',
              icon: 'success',
              duration: 2000,
              success: function(){
                that.setData({ showScore: false });
                setTimeout(function(){
                    that.refreshData();
                }, 1800);
              }
          });
        }
      }
    });
  },
  openDetails: function(e){
    wx.navigateTo({
        url: '/pages/feedbacks/feedbackDetails/feedbackDetails?id=' + e.currentTarget.dataset.id
    });
  },
  btnDelete: function(e){
    var that = this;
    wx.showModal({
      title: '提示：',
      content: '你确定要 删除 此反馈吗？',
      success: function (res) {
        if (res.confirm) {
            API.removeById({
              method: 'POST',
              header: {
                'Content-Type': 'application/json',
                userAuth: wx.getStorageSync("openId"),
              },
              fbId: e.currentTarget.dataset.fbid,
              success: function (data) {
                if (data.data.data == 1) {
                    wx.showToast({
                      title: '反馈 删除 成功！',
                      icon: 'success',
                      duration: 2000
                    });
                    that.refreshData();
                }else{
                    wx.showModal({
                      title: '提示：',
                      content: '反馈 删除 失败！',
                      showCancel: false
                    });
                }
              }
            });
        }
      }
    });
  },
  btnScore: function (e) {
    this.setData({ showScore: true, fbid: e.currentTarget.dataset.fbid, key: 0});
  },
  //反馈列表
  refreshData:function(){
    var that = this;
    API.findFeedbacksByParentId({
        method: 'POST',
        header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId"),
        },
        data: {
          conditions: {},
          pageNumber: 1,
          pageSize: this.data.pageSize,
          sort: "feedbackTime",
          dir: "DESC"
        },
        success:function(data){
            if(data.statusCode == 200){
                for(var i=0;i < data.data.data.content.length;i++){
                    var feedbackTime = new Date(data.data.data.content[i].feedbackTime);
                    data.data.data.content[i].feedbackTime = feedbackTime.Format('yyyy-MM-dd hh:mm:ss');
                    if (data.data.data.content[i].score != undefined){
                        data.data.data.content[i].score = parseInt(data.data.data.content[i].score);
                    }
                }
                if (data.data.data.pageSize <= 10){
                  that.setData({
                    servicesData: data.data.data.content,
                    totalPage: data.data.data.totalPage,
                    isLoading: false,
                    isError: false
                  });
                }else{
                  that.setData({
                    servicesData: data.data.data.content,
                    isLoading: false,
                    isError: false
                  });
                }
                
                //如果没有反馈，跳转到添加反馈
                if (that.data.isFirst == true) {
                  that.setData({isFirst: false});
                  if (data.data.data.content.length <= 0){
                      setTimeout(function(){
                        that.addFeedback();
                      }, 500);
                  }
                }
            }
        },
        fail: function (err) {
          that.setData({ isLoading: false, isError: true });
        }
    });
  },
  //添加反馈
  addFeedback: function(){
      wx.navigateTo({
          url: '/pages/feedbacks/addFeedback/addFeedback'
      });
  },
  makePhoneCall: function () {
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  }
})
