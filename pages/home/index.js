//index.js
import api from '../../api/api.js';
import utils from '../../utils/util.js';
Page({
  data: {
    imgHeight: 0,
    imgWidth: 84,
    viewAndroid: "",
    platform: "",
    serviceData: [],
    pageSize: 4,
    totalPage: 0,
    stickTop: 0,
    scrollHeight: "0px",
    viewHeight: '0px',
    showStickTop: false,
    isNetOff: false,
    showWhetherBind: false,
    isLoading: true,
    carouselList: [],
    articlesCount: 0,
    displayShow: false
  },
  bindScroll: function(e){
    if(e.detail.scrollTop >= 30){
      this.setData({ showStickTop: true });  
    }else{
      this.setData({ showStickTop: false });
    }
  },
  bindStickTop: function(){
    this.setData({ stickTop: 0, showStickTop: false });
  },
  onImgCarouselLoad: function(e){
    this.data.carouselList[e.currentTarget.id].bgPicture = "background-image:none";
    this.setData({
      carouselList: this.data.carouselList
    });
  },
  onImgLoad: function (e) {
    this.data.serviceData[e.currentTarget.id].bgPicture = "background-image:none";
    this.setData({
      serviceData: this.data.serviceData
    });
  },
  bindLogin: function(){
    var that = this;
    var bugIsNetOff = setInterval(function () {
      that.setData({ isNetOff: false, isLoading: true, showWhetherBind: false });
    }, 1);
    wx.login({
      success: function (res) {
        if (res.code) {
          api.whetherBind({
            code: res.code,
            success: function (data) {
              if (data.statusCode != 200) {
                wx.showModal({
                  title: '提示',
                  content: "对不起，链接断开。",
                  showCancel: false,
                });
                return;
              }
              if (data.data.code == 0) {
                wx.setStorageSync("openId", data.data.data);
                wx.redirectTo({
                  url: '/pages/binding/binding?flag=true',
                });
              } else {
                wx.setStorageSync("openId", data.data.data);
              }
              that.setData({ showWhetherBind: false });
            },
            fail: function (err) {
              console.log(JSON.stringify(err));
              that.setData({ showWhetherBind: true });
              wx.showModal({
                title: '提示',
                content: "对不起，链接问题。",
                showCancel: false,
              });
            }
          });
        }
      },
      fail: function (err) {
        wx.showModal({
          title: '提示',
          content: "对不起，链接问题。",
          showCancel: false,
        });
      }
    });
    var interval = setInterval(function () {
      if (wx.getStorageSync("openId") != "") {
        clearInterval(bugIsNetOff); clearInterval(interval);
        that.refreshData();
        that.findCarousels();
      }
    }, 500);
  },
  getUpdateArticles: function(){
    var times = new Date(); var that = this;
    var subTimes = new Date(times.getTime() - 1000 * 60 * 60 * 24 * 6);
    var startTime = subTimes.getFullYear() + "-" + (((subTimes.getMonth() + 1) < 10) ? "0" + (subTimes.getMonth() + 1) : (subTimes.getMonth() + 1)) + "-" + ((subTimes.getDate() < 10) ? "0" + subTimes.getDate() : subTimes.getDate()) + " 00:00:00";
    var endTime = times.getFullYear() + "-" + (((times.getMonth() + 1) < 10) ? "0" + (times.getMonth() + 1) : (times.getMonth() + 1)) + "-" + ((times.getDate() < 10) ? "0" + times.getDate() : times.getDate()) + " 23:59:59";

    api.recentUpdateArticles({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: {
        startTime: startTime,
        endTime: endTime
      },
      success: function (data) {
        if (data.data.code == 1) {
          if (data.data.data > 0){
              that.setData({ displayShow: true });
              var animation = wx.createAnimation({
                duration: 500,
                timingFunction: 'ease',
              }).opacity(1).width('100%').height(26).step();

              that.setData({
                animationData: animation.export(),
                articlesCount: data.data.data
              });

              setTimeout(function () {
                var animation = wx.createAnimation({
                  duration: 800,
                  timingFunction: 'ease',
                }).opacity(0).width(0).height(0).step();
                that.setData({
                  animationData: animation.export()
                });
                setTimeout(function () {
                  that.setData({ displayShow: false });
                }, 800);
              }, 3000);
          }
        }
      }
    })
  },
  onLoad: function(){
    var that = this;
    var bugIsNetOff = setInterval(function () {
      that.setData({ isNetOff: false, isLoading: true });
    }, 1);
    wx.getSystemInfo({
      success: function (res) {
        that.setData({ viewHeight: res.windowHeight + "px"});
      }
    })
    if (wx.getStorageSync("tencent") == false){
        wx.login({
          success: function (res) {
            if (res.code) {
              api.whetherBind({
                code: res.code,
                success: function (data) {
                  if (data.statusCode != 200) {
                    wx.showModal({
                      title: '提示',
                      content: "对不起，链接断开。",
                      showCancel: false,
                    });
                    that.setData({ showWhetherBind: true });
                    return;
                  }
                  if (data.data.code == 0) {
                    wx.setStorageSync("openId", data.data.data);
                    wx.redirectTo({
                      url: '/pages/binding/binding?flag=true',
                    });
                  } else {
                    wx.setStorageSync("openId", data.data.data);
                    setTimeout(function () {
                      that.getUpdateArticles();
                    }, 2600);
                  }
                },
                fail: function (err) {
                  console.log(JSON.stringify(err));
                  that.setData({ showWhetherBind: true});
                }
              });
            }
          },
          fail: function(err){
            that.setData({ showWhetherBind: true });
          }
        });
    }
    var interval = setInterval(function () {
      if (wx.getStorageSync("openId") != ""){
        clearInterval(bugIsNetOff); clearInterval(interval);
        that.refreshData();
        that.findCarousels();
      }
    }, 500);
  },
  onShow: function(){
    this.refreshData();
    this.findCarousels();
  },
  findCarousels: function(){
    if (wx.getStorageSync("openId") == ""){return;}
    var that = this;
    api.findCarousels({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: {
        app: 'app'
      },
      success: function (data) {
        if (data.data.code == 1) {
          for (var i = 0; i < that.data.carouselList.length; i++) {
            if (that.data.carouselList[i].bgPicture != undefined) {
              if (data.data.data[i] != undefined) {
                data.data.data[i].bgPicture = that.data.carouselList[i].bgPicture;
              } else {
                break;
              }
            }
          }
          that.setData({
            carouselList: data.data.data
          });
        }
      }
    });
  },
  refreshData: function(){
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        var tempWidth = parseInt((res.windowWidth - 40) / 4);
        var tempHeight = parseInt(res.windowWidth / 1.96296); //1.637755
        that.setData({
          imgHeight: tempHeight,
          imgWidth: (tempWidth + 1),
          scrollHeight: res.windowHeight + "px"
        });
        if (res.platform == "android" || res.platform == "devtools") {
          that.setData({ platform: res.platform, viewAndroid: "viewContent viewAndroid" });
        } else {
          that.setData({ platform: res.platform, viewAndroid: "viewContent1" });
        }
      }
    });
    if (wx.getStorageSync("openId") != "") {
      api.parentProfile({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success: function (data) {
          if (data.data.code == 1) {
            if (data.data.data != undefined){
              if (data.data.data.openId != undefined && data.data.data.openId != "") {
                wx.setStorageSync("decryptedOpenId", data.data.data.openId);
              }
            }
          }
        }
      });
    }

    if (wx.getStorageSync("openId") != "") {
      api.findExtensions({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        data: {
          conditions: {
            extension: 'true'
          },
          pageNumber: 1,
          pageSize: this.data.pageSize,
          sort: "extensionTime",
          dir: "DESC"
        },
        success: function (data) {
          if (data.statusCode != 200){
            wx.hideToast();
            that.setData({ isLoading: false, isNetOff: true, serviceData: [], totalPage: 0 });
            return;
          }
          if (data.data.code == 1) {
            for (var i = 0; i < data.data.data.content.length; i++) {
              data.data.data.content[i].extensionTime = new Date(data.data.data.content[i].extensionTime).Format("MM 月 dd 日");
              data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
            }
            for (var i = 0; i < that.data.serviceData.length; i++) {
              if (that.data.serviceData[i].bgPicture != undefined) {
                if (data.data.data.content[i] != undefined){
                  data.data.data.content[i].bgPicture = that.data.serviceData[i].bgPicture;
                }else{
                  break;
                }
              }
            }
            that.setData({
              serviceData: data.data.data.content,
              totalPage: data.data.data.totalPage,
              isNetOff: false,
              isLoading: false
            });
            
          }
        },
        fail: function (err) {
          wx.hideToast();
          that.setData({ isLoading: false, isNetOff: true, serviceData: [], totalPage: 0 });
        }
      });
    }
  },
  bindRefreshData: function(){
    var that = this;
    that.findCarousels();
    wx.showToast({
      title: '加载中...',
      icon: 'loading',
      duration: 10000,
      mask: true
    });
    api.findExtensions({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: {
        conditions: {
          extension: 'true'
        },
        pageNumber: 1,
        pageSize: this.data.pageSize,
        sort: "extensionTime",
        dir: "DESC"
      },
      success: function (data) {
        if (data.statusCode != 200) {
          wx.hideToast();
          wx.showModal({
            title: '提示：',
            content: "链接错误，点击重试。",
            showCancel: false
          });
          that.setData({ isNetOff: true, serviceData: [], totalPage: 0, isLoading: false });
          return;
        }
        wx.hideToast();
        if (data.data.code == 1) {
          for (var i = 0; i < data.data.data.content.length; i++) {
            data.data.data.content[i].extensionTime = new Date(data.data.data.content[i].extensionTime).Format("MM 月 dd 日");
            data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
          }
          for (var i = 0; i < that.data.serviceData.length; i++) {
            if (that.data.serviceData[i].bgPicture != undefined) {
              if (data.data.data.content[i] != undefined) {
                data.data.data.content[i].bgPicture = that.data.serviceData[i].bgPicture;
              }else{
                break;
              }
            }
          }
          that.setData({
            serviceData: data.data.data.content,
            totalPage: data.data.data.totalPage,
            isNetOff: false,
            isLoading: false
          });
        }
      },
      fail: function (err) {
        wx.hideToast();
        that.setData({ isNetOff: true, serviceData: [], totalPage: 0, isLoading: false });
        wx.showModal({
          title: '提示：',
          content: "网络链接错误，点击重试。",
          showCancel: false
        });
      }
    });
  },
  scrollPageInfo: function(){
    var that = this;
    if (this.data.totalPage > parseInt(this.data.pageSize/4)){
        this.data.pageSize += 4;
        api.findExtensions({
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId"),
          },
          data: {
            conditions: {
              extension: 'true'
            },
            pageNumber: 1,
            pageSize: this.data.pageSize,
            sort: "extensionTime",
            dir: "DESC"
          },
          success: function (data) {
            if (data.data.code == 1) {
              for (var i = 0; i < data.data.data.content.length; i++) {
                data.data.data.content[i].extensionTime = new Date(data.data.data.content[i].extensionTime).Format("MM 月 dd 日");
                data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
              }
              for (var i = 0; i < that.data.serviceData.length; i++) {
                if (that.data.serviceData[i].bgPicture != undefined) {
                  if (data.data.data.content[i] != undefined) {
                      data.data.data.content[i].bgPicture = that.data.serviceData[i].bgPicture;
                  }else{
                      break;
                  }
                }
              }
              that.setData({
                serviceData: data.data.data.content,
                totalPage: data.data.data.totalPage,
                isNetOff: false
              });
            }
          },
          fail: function (err) {
            wx.hideToast();
            that.setData({ isNetOff: true, serviceData: [], totalPage: 0 });
          }
        });
    }
  },
  openActivitiesDetails: function (e) {
    wx.navigateTo({
      url: '/pages/articleList/articleDetails/articleDetails?id=' + e.currentTarget.dataset.id + "&likeCount=" + e.currentTarget.dataset.likecount + "&title=" + e.currentTarget.dataset.title
    });
  },
  openCarouselDetails: function (e) {
    if (e.currentTarget.dataset.id == undefined){return;}
    wx.showToast({ title: '打开中', icon: 'loading', mask: true, duration:10000});
    api.findArticleDetial({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      id: e.currentTarget.dataset.id,
      success: function (data) {
        if (data.data.code == 1) {
          wx.hideToast();
          var tempData = data.data.data;
          wx.navigateTo({
            url: '/pages/articleList/articleDetails/articleDetails?id=' + tempData.article.id + "&likeCount=" + tempData.article.likeIds.length + "&title=" + tempData.article.title
          });
        }
      },
      fail: function (err) {
        wx.hideToast();
        wx.showModal({
          title: '提示：',
          content: "网络链接错误，点击重试。",
          showCancel: false
        });
      }
    });
  },
  gotoNavigatorUrl: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  onShareAppMessage: function () {
    return {
      title: '您孩子的在美学生报告自助查询',
      desc: '您孩子的在美学生报告自助查询',
      path: '/pages/home/index'
    }
  },
  formSubmitByFormId: function(e){
    utils.sendFormIdAndGatherFormId(e);
  }
})
