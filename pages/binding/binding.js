import api from '../../api/api.js'
Page({
  data: {
    date: '2016-09-01',
    foc_class1:'orclass',
    foc_class2:'orclass',
    foc_class3:'orclass',
    foc_class4:'orclass',
    foc_class5:'orclass',
    loading1:false,
    loading2:false,
    loading1Notice:false,
    countDown:0,
    btnText: '获取',
    objectArray: [
      {
        key: '父亲Father',
        value: 'father',
      },
      {
        key: '母亲Mother',
        value: 'mother',
      },
      {
        key: '兄弟姐妹Sibling',
        value: 'sibling',
      },
      {
        key: '祖父母Grandparents',
        value: 'grandparents',
      }
      // {
      //   key: '学生本人Myself',
      //   value: 'myself',
      // }
    ],
    index: 0,
    firstName: '',
    lastName: '',
    cellphone: '',
    validateCode: '',
    noticeCode: '',
    originalNoticeCode: '',
    flag: "true",
    disabled: true,
    disabledNotice: true,
    isBindDisabled: true,
    btnHelp: false,
    btnDisabledHelp: false,
    btnLoadingHelp: false,
    formId: null,
    swiperCode: true,
    serverValidCode: ''
  },
  onLoad :function(option){
      this.data.flag = option.flag;
      if (option.flag == "true" && option.validateCode == undefined){
        wx.showModal({
          title: '提示',
          content: '请绑定学生！',
          showCancel:false
        })
      }

      if (option.validateCode != undefined){
        var relationshipIndex  = 0;
        if (option.relationship == "father"){
          relationshipIndex = 0;
        } else if (option.relationship == "mother") {
          relationshipIndex = 1;
        } else if (option.relationship == "sibling") {
          relationshipIndex = 2;
        } else if (option.relationship == "grandparents") {
          relationshipIndex = 3;
        }
        // this.setData({ 
        //   noticeCode: option.validateCode,
        //   originalNoticeCode: option.validateCode,
        //   firstName: option.firstName,
        //   lastName: option.lastName,
        //   date: option.dateOfBirth,
        //   cellphone: option.cellphone,
        //   index: relationshipIndex,
        //   isBindDisabled: true,
        //   loading2: true,
        //   disabledNotice: false,
        //   disabled: false,
        //   swiperCode: false,
        // });
        // this.autoSubmit();

        // 通知验证码，手动绑定
        this.setData({
          noticeCode: option.validateCode,
          originalNoticeCode: option.validateCode,
          firstName: option.firstName,
          lastName: option.lastName,
          date: option.dateOfBirth,
          cellphone: option.cellphone,
          index: relationshipIndex,
          isBindDisabled: false,
          loading2: false,
          disabledNotice: false,
          disabled: false,
          swiperCode: false,
        });
      }
      if (wx.getStorageSync("tencent") == ""){
        wx.setStorageSync("tencent", false);
      }

      //每五秒，刷新一下，看工作人员是否绑定，绑定跳转首页。
      setInterval(function(){
        if (wx.getStorageSync("wxCode") != "" && option.flag == "true"){
          wx.login({
            success: function (res) {
              api.whetherBind({
                code: res.code,
                success: function (data) {
                  if (data.statusCode != 200) {
                    return;
                  }
                  if (data.data.code != 0) {
                    wx.removeStorageSync("wxCode");
                    wx.setStorageSync("openId", data.data.data);

                    if (wx.reLaunch) {
                      wx.reLaunch({ url: '/pages/home/index' });
                    } else {
                      wx.switchTab({ url: '/pages/home/index' });
                    }

                    wx.showModal({
                      title: '提示',
                      content: '工作人员已给你绑定，请到学生列表查看。',
                      showCancel: false
                    })
                  }
                },
                fail: function (err) {
                  console.log("对不起，链接问题。");
                }
              });
            }
          });
        }
      },5000);
  },
  //日期选择，回调
  bindDateChange: function(e) {
    this.setData({
      date: e.detail.value
    })
  },
  //与学生的关系，回调
  bindclass1: function(e){
     this.setData({
      foc_class1: "foc_input"
    })
  },
  rebindclass1: function(e){
     this.setData({
      foc_class1: "orclass"
    })
  },
  bindclass2: function(e){
     this.setData({
      foc_class2: "foc_input"
    })
  },
  rebindclass2: function(e){
     this.setData({
      foc_class2: "orclass"
    })
  },
  bindclass3: function(e){
     this.setData({
      foc_class3: "foc_input"
    })
  },
  rebindclass3: function(e){
     this.setData({
      foc_class3: "orclass"
    })
  },
  bindclass4: function(e){
     this.setData({
      foc_class4: "foc_input"
    })
  },
  rebindclass4: function(e){
     this.setData({
      foc_class4: "orclass"
    })
  },
  bindclass5: function(e){
     this.setData({
      foc_class5: "foc_input"
    })
  },
  rebindclass5: function(e){
     this.setData({
      foc_class5: "orclass"
    })
  },
  bindclass6: function(e){
     this.setData({
      foc_class6: "foc_input"
    })
  },
  rebindclass6: function(e){
     this.setData({
      foc_class6: "orclass"
    })
  },
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },
  bindFirstNameInput: function(e) {
    this.data.firstName = e.detail.value;
    this.isShowValidateBtn();
  },
  bindLastNameInput: function(e) {
    this.data.lastName = e.detail.value;
    this.isShowValidateBtn();
  },
  bindCellphoneInput: function(e) {
    if (/^[\d\s-+]*$/.test(e.detail.value) == false) {
      return {
        value: this.data.cellphone.replace(e.detail.value, "")
      }
    }
    this.data.cellphone = e.detail.value;
    this.isShowValidateBtn();
  },
  bindValidateCodeInput: function (e) {
    this.data.validateCode = e.detail.value;
    this.isShowValidateBtn();
  },
  bindNoticeCodeInput: function (e) {
    this.data.noticeCode = e.detail.value;
    this.isShowValidateBtn();
  },
  isShowValidateBtn: function (){
    if (this.data.loading1 == false && this.data.loading2 == false ){
      if (this.data.firstName.trim() == "") {
        this.setData({ disabled: true, disabledNotice: true, isBindDisabled: true });
        return;
      }
      if (this.data.lastName.trim() == "") {
        this.setData({ disabled: true, disabledNotice: true, isBindDisabled: true });
        return;
      }
      if (this.data.cellphone == "") {
        this.setData({ disabled: true, disabledNotice: true, isBindDisabled: true });
        return;
      }
      if (this.data.validateCode.length >= 4) {
        this.setData({isBindDisabled: false });
        if (this.data.countDown <= 0){
          this.setData({ disabled: false });
        }
      } else {
        if (this.data.noticeCode.length >= 4){
          this.setData({ isBindDisabled: false });
        }else{
          this.setData({ isBindDisabled: true });
        }
        
        if (this.data.countDown <= 0) {
          this.setData({ disabled: false });
        }
      }
      this.setData({ disabledNotice: false});
    }
  },
  //获取验证码
  getVerifCode: function(){
    if (/^1[3,4,5,7,8]\d{9}$/.test(this.data.cellphone.trim()) == false) {
      wx.showModal({
        title: '提示',
        content: '手机号码 格式不正确。',
        showCancel: false
      });
      this.setData({btnHelp: true});
      return;
    }
    var page=this
    page.setData({
      loading1: true,
      disabled: true,
      countDown: 60
    })
    api.getAuthenticationCode({
        method: 'POST',
        data:{
            notifyOrSMS: 'SMS',
            firstName: this.data.firstName.trim(),
            lastName: this.data.lastName.trim(),
            cellphone: this.data.cellphone.trim(),
            dateOfBirth: this.data.date, 
            relationship: this.data.objectArray[this.data.index].value
        },
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success :function(data){
            if(data.data.data == "1"){
                wx.showModal({
                  title: '提示',
                  content: '短信发送，请查收。',
                  showCancel: false
                });
                var tempInterval = setInterval(function () {
                  if (page.data.countDown > 0) {
                    page.data.countDown--;
                    page.setData({
                      loading1: false,
                      countDown: page.data.countDown,
                      btnText: page.data.countDown + '秒'
                    });
                  } else {
                    page.setData({
                      loading1: false,
                      disabled: false,
                      btnText: '获取'
                    });
                    clearInterval(tempInterval);
                  }
                }, 1000);
            } else if(data.data.data == "0"){
                wx.showModal({
                  title: '提示',
                  content: '发送验证码失败，请稍后再试。',
                  showCancel: false
                });
            }else {
                wx.showModal({
                  title: '提示',
                  content: data.data.data,
                  showCancel: false
                });
                page.setData({
                  loading1: false,
                  btnText: '获取',
                  btnHelp: true,
                  disabled: false
                });
            }
            page.isShowValidateBtn();
        },
        fail: function(err){
            page.isShowValidateBtn();
            wx.showModal({
              title: '提示',
              content: '对不起，服务器请求错误！',
              showCancel: false
            });
            page.setData({
              loading1: false,
              disabled: false,
              btnText: '获取'
            })
        }
    })
  },
  formSubmit: function (e) {
    var that = this;
    this.data.formId = e.detail.formId;
    this.data.firstName = e.detail.value.firstName.trim();
    this.data.lastName = e.detail.value.lastName.trim();
    this.data.cellphone = e.detail.value.cellphone.trim();

    if (e.detail.target.dataset.flag == "true") {
      this.setData({ disabledNotice: true, loading1Notice: true });
      api.getAuthenticationCode({
        method: 'POST',
        data: {
          formId: this.data.formId,
          flag: this.data.flag,
          notifyOrSMS: 'notify',
          firstName: that.data.firstName.trim(),
          lastName: that.data.lastName.trim(),
          cellphone: that.data.cellphone.trim(),
          dateOfBirth: this.data.date,
          relationship: this.data.objectArray[this.data.index].value
        },
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success: function (data) {
          if (data.data.code == 1) {
            if (data.data.data.length != 6) {
              that.setData({ btnHelp: true });
              wx.showModal({
                title: '提示',
                content: '对不起，您填写的信息有误!',
                showCancel: false
              });
            } else if (data.data.data.length == 6) {
              wx.showModal({
                title: '提示',
                content: '验证码已通过微信服务通知发送，请查收。',
                showCancel: false
              });
            }
          }
          that.setData({ disabledNotice: false, loading1Notice: false });
        },
        fail: function (err) {
          that.setData({ disabledNotice: false, loading1Notice: false });
          wx.showModal({
            title: '提示',
            content: '链接断开，请检查网络。',
            showCancel: false
          });
        }
      });
    }
  },
  getUserInfo(e) {
    var that = this;
    if (e.detail.errMsg == "getUserInfo:fail auth deny") {
      that.Reauthorization();
    } else {
      //是否是推送通知（false：推送验证码）
      //-------------------------------腾讯测试用-----------------------------
      if (/^1[3,4,5,7,8]\d{9}$/.test(that.data.cellphone.trim()) == false) {
        wx.showModal({
          title: '提示',
          content: '手机号码 格式不正确。',
          showCancel: false
        });
        this.setData({ btnHelp: true });
        return;
      }

      var tencentTestPhone = getApp().globalData.tencentTestPhone;
      var tencentTestVerifiCode = getApp().globalData.tencentTestVerifiCode;
      if (that.data.cellphone.trim() == tencentTestPhone && that.data.validateCode == tencentTestVerifiCode) {
        that.setData({ isBindDisabled: true });
        api.tencentTest({
          method: 'POST',
          success: function (data) {
            if (data.data.code == 0) {
              wx.setStorageSync("openId", data.data.data);
              wx.showModal({
                title: '提示',
                content: '请绑定学生。',
                showCancel: false
              });
            } else {
              wx.setStorageSync("openId", data.data.data);
              wx.setStorageSync("tencent", true);
              if (wx.reLaunch) {
                wx.reLaunch({ url: '/pages/home/index' });
              } else {
                wx.switchTab({ url: '/pages/home/index' });
              }
            }
            that.isShowValidateBtn();
          },
          fail: function (err) {
            that.isShowValidateBtn();
            wx.showModal({
              title: '提示',
              content: '链接断开，请检查网络。',
              showCancel: false
            });
          }
        });
        return;
      }
      //----------------------------------腾讯测试用----------------------------
      
      that.setData({ loading2: true, isBindDisabled: true });
      var notifyOrSMS = "";
      if (this.data.swiperCode == true) { //手机验证码
        notifyOrSMS = "SMS";
        that.bindStudentInfo(e.detail.userInfo, that, notifyOrSMS);
      } else { //通知验证码
        api.verificationNotificationCode({
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId"),
          },
          success: function (data) {
            if (data.data.code == 1) {
              if (data.data.data.toString() == "2") {
                wx.showModal({
                  title: '提示',
                  content: "通知验证码失效，请重新获取!",
                  showCancel: false
                });
                that.setData({ loading2: false, isBindDisabled: false });
              } else {
                if (data.data.data != that.data.noticeCode) {
                  wx.showModal({
                    title: '提示',
                    content: "通知验证码不匹配，请重新获取!",
                    showCancel: false
                  });
                  that.setData({ loading2: false, isBindDisabled: false });
                } else {
                  notifyOrSMS = "notify";
                  that.bindStudentInfo(e.detail.userInfo, that, notifyOrSMS);
                }
              }
            }
          },
          fail: function (err) {
            that.setData({ loading2: false, isBindDisabled: false });
            wx.showToast({
              title: '链接断开',
              icon: 'success',
              duration: 2000
            });
          }
        })
      }
    }
  },
  bindStudentInfo: function (userInfo, page, notifyOrSMS){
    console.log(JSON.stringify(
      {
        cellphone: page.data.cellphone.trim(),
        relationship: page.data.objectArray[page.data.index].value,
        notifyOrSMS: notifyOrSMS,
        firstName: page.data.firstName.trim(),
        lastName: page.data.lastName.trim(),
        dateOfBirth: page.data.date,
        formId: page.data.formId
      }
    ));

    if (page.data.flag == "false") {
      var paramData = {
          cellphone: page.data.cellphone.trim(),
          relationship: page.data.objectArray[page.data.index].value,
          notifyOrSMS: notifyOrSMS,
          firstName: page.data.firstName.trim(),
          lastName: page.data.lastName.trim(),
          dateOfBirth: page.data.date,
          formId: page.data.formId
      }
      if (notifyOrSMS == "SMS") {
          paramData.validateCode = page.data.validateCode
      }
      api.bindStudent({
        method: 'POST',
        data: paramData,
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success: function (data) {
          console.log(JSON.stringify(data));
          if (data.data.code == 1) {
            if(data.data.data.status == 1){
                wx.showModal({
                  title: '提示',
                  content: '恭喜你，绑定成功。',
                  showCancel: false,
                  success: function (res) {
                    if (wx.reLaunch) {
                      wx.reLaunch({ url: '/pages/listOfStudent/bindStudent' });
                    } else {
                      wx.switchTab({ url: '/pages/listOfStudent/bindStudent' });
                    }
                  }
                });
            } else if (data.data.data.status == 0) {
                if (data.data.data.message.includes("你已经绑定了学生") == true){
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false,
                    success: function (res) {
                      if (res.confirm) {
                        wx.reLaunch({ url: '/pages/listOfStudent/bindStudent' });
                      }
                    }
                  });
                }else{
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false
                  });
                }
            } else if (data.data.data.status == 2) {
                wx.showModal({
                  title: '提示',
                  content: data.data.data.message,
                  showCancel: false
                });
            }
            page.setData({ loading2: false, isBindDisabled: false });
          } else {
            page.setData({ isBindDisabled: false, loading2: false, btnHelp: true });
          }
          page.isShowValidateBtn();
        },
        fail: function (err) {
          page.isShowValidateBtn();
          wx.showToast({
            title: '链接断开',
            icon: 'success',
            duration: 2000
          });
        }
      });
    } else {
      var paramData = {
          //授权信息
          nickName: userInfo.nickName,
          gender: userInfo.gender,
          city: userInfo.city,
          province: userInfo.province,
          country: userInfo.country,
          avatarUrl: userInfo.avatarUrl,

          //学生 绑定信息
          cellphone: page.data.cellphone.trim(),
          relationship: page.data.objectArray[page.data.index].value,
          notifyOrSMS: notifyOrSMS,
          firstName: page.data.firstName.trim(),
          lastName: page.data.lastName.trim(),
          dateOfBirth: page.data.date,
          formId: page.data.formId
      }
      if (notifyOrSMS == "SMS") {
          paramData.validateCode = page.data.validateCode
      }
      api.bindStudent({
        method: 'POST',
        data: paramData,
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success: function (data) {
          if (data.data.code == 1) {
            if (data.data.data.status == 1) {
              wx.showModal({
                title: '提示',
                content: '恭喜你，绑定成功。',
                showCancel: false,
                success: function (res) {
                  if (wx.reLaunch) {
                    wx.reLaunch({ url: '/pages/home/index' });
                  } else {
                    wx.switchTab({ url: '/pages/home/index' });
                  }
                }
              });
            } else if (data.data.data.status == 0) {
              if(data.data.data.message.includes("你已经绑定了学生") == true){
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false,
                    success: function (res) {
                      if (res.confirm) {
                        wx.reLaunch({ url: '/pages/listOfStudent/bindStudent' });
                      }
                    }
                  });
                }else{
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false
                  });
              }
            } else if (data.data.data.status == 2) {
              wx.showModal({
                title: '提示',
                content: data.data.data.message,
                showCancel: false
              });
            }
            page.setData({ loading2: false, isBindDisabled: false });
          } else {
            page.setData({ isBindDisabled: false, loading2: false, btnHelp: true });
          }
          page.isShowValidateBtn();
        },
        fail: function (err) {
          page.isShowValidateBtn();
          wx.showToast({
            title: '链接断开',
            icon: 'success',
            duration: 2000
          });
        }
      });
    }
  },
  Reauthorization: function (){
    var page = this;
    wx.openSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          //已经授权用户
          wx.showModal({
            title: '提示：',
            content: '请再次点击按钮。',
            showCancel: false
          })
        } else {
          //未授权，提示
          wx.showModal({
            title: '提示：',
            content: '请授权才能使用。',
            showCancel: false,
            success: function (res) {
              page.Reauthorization();
            }
          })
        }
      }
    });
  },
  btnHelp: function(e){
    var that = this;
    if (e.detail.errMsg == "getUserInfo:fail auth deny") {
      that.Reauthorization();
    } else {
      wx.showModal({
        title: '提示：',
        content: '请你确认以下信息：\n\n姓氏：' + this.data.firstName.trim() + '\n名字：' + this.data.lastName.trim() + '\n出生日期：' + this.data.date + '\n与学生关系：' + this.data.objectArray[this.data.index].key + '\n手机号：' + this.data.cellphone.trim() + '\n\n已经确定，提交工作人员？',
        showCancel: true,
        success: function (res) {
          if (res.confirm == true) {
            that.setData({ btnDisabledHelp: true, btnLoadingHelp: true });
            wx.login({
              success: function (loginRes) {
                wx.setStorageSync("wxCode", "help");
                if (loginRes.code) {
                  var userInfo = e.detail.userInfo;
                  api.authorizationBinding({
                    method: 'POST',
                    code: loginRes.code,
                    data: {
                      //授权信息
                      nickName: userInfo.nickName,
                      gender: userInfo.gender,
                      city: userInfo.city,
                      province: userInfo.province,
                      country: userInfo.country,
                      avatarUrl: userInfo.avatarUrl,

                      firstName: that.data.firstName.trim(),
                      lastName: that.data.lastName.trim(),
                      cellphone: that.data.cellphone.trim(),
                      dateOfBirth: that.data.date,
                      relationship: that.data.objectArray[that.data.index].value
                    },
                    success: function (data) {
                      that.setData({ btnDisabledHelp: false, btnLoadingHelp: false });
                      wx.showModal({
                        title: '提示：',
                        content: data.data.data,
                        showCancel: false
                      });
                    },
                    fail: function (err) {
                      that.setData({ btnDisabledHelp: false, btnLoadingHelp: false });
                      wx.showToast({
                        title: '链接断开',
                        icon: 'success',
                        duration: 2000
                      });
                    }
                  });
                }
              },
              fail: function (err) {
                wx.showModal({
                  title: '提示',
                  content: "对不起，链接问题。",
                  showCancel: false,
                });
              }
            });
          }
        }
      })
    }
  },
  autoSubmit: function () {
    var page = this;
    //-------------------------------腾讯测试用-----------------------------
    var tencentTestPhone = getApp().globalData.tencentTestPhone;
    var tencentTestVerifiCode = getApp().globalData.tencentTestVerifiCode;
    if (this.data.cellphone.trim() == tencentTestPhone && this.data.validateCode == tencentTestVerifiCode) {
      page.setData({ isBindDisabled: true });
      api.tencentTest({
        method: 'POST',
        success: function (data) {
          if (data.data.code == 0) {
            wx.setStorageSync("openId", data.data.data);
            wx.showModal({
              title: '提示',
              content: '请绑定学生。',
              showCancel: false
            });
          } else {
            wx.setStorageSync("openId", data.data.data);
            wx.setStorageSync("tencent", true);
            if (wx.reLaunch) {
              wx.reLaunch({ url: '/pages/home/index' });
            } else {
              wx.switchTab({ url: '/pages/home/index' });
            }
          }
          page.isShowValidateBtn();
        },
        fail: function (err) {
          page.isShowValidateBtn();
          wx.showModal({
            title: '提示',
            content: '链接断开，请检查网络。',
            showCancel: false
          });
        }
      });
      return;
    }
    //----------------------------------腾讯测试用----------------------------
    wx.showLoading({
      title: '验证通知码',
      mask: true
    });
    var notifyOrSMS = "notify";
    api.verificationNotificationCode({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      success: function (data) {
        if (data.data.code == 1) {
          if (data.data.data.toString() == "2") {
            wx.showModal({
              title: '提示',
              content: "通知验证码失效，请重新获取!",
              showCancel: false
            });
            wx.hideLoading();
            page.setData({ loading2: false, isBindDisabled: false });
          } else {
            if (data.data.data != page.data.noticeCode) {
              wx.showModal({
                title: '提示',
                content: "通知验证码不匹配，请重新获取!",
                showCancel: false
              });
              wx.hideLoading();
              page.setData({ loading2: false, isBindDisabled: false });
            } else {
              page.bindStudentInfoByAuto(page, notifyOrSMS);
            }
          }
        }
      },
      fail: function (err) {
        wx.hideLoading();
        page.setData({ loading2: false, isBindDisabled: false });
        wx.showModal({
          title: '提示',
          content: "链接断开!",
          showCancel: false
        });
      }
    })
  },
  bindStudentInfoByAuto: function (page, notifyOrSMS) {
    wx.showLoading({ title: '绑定中，请稍候', mask: true });
    if (page.data.flag == "false") {
      var paramData = {
        cellphone: page.data.cellphone.trim(),
        relationship: page.data.objectArray[page.data.index].value,
        notifyOrSMS: notifyOrSMS,
        firstName: page.data.firstName.trim(),
        lastName: page.data.lastName.trim(),
        dateOfBirth: page.data.date
      }
      api.bindStudent({
        method: 'POST',
        data: paramData,
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        success: function (data) {
          if (data.data.code == 1) {
            if (data.data.data.status == 1) {
              wx.showModal({
                title: '提示',
                content: '恭喜你，绑定成功。',
                showCancel: false,
                success: function (res) {
                  if (wx.reLaunch) {
                    wx.reLaunch({ url: '/pages/listOfStudent/bindStudent' });
                  } else {
                    wx.switchTab({ url: '/pages/listOfStudent/bindStudent' });
                  }
                }
              });
            } else if (data.data.data.status == 0) {
              wx.showModal({
                title: '提示',
                content: data.data.data.message,
                showCancel: false
              });
            } else if (data.data.data.status == 2) {
              wx.showModal({
                title: '提示',
                content: data.data.data.message,
                showCancel: false
              });
            } 
            wx.hideLoading();
            page.setData({ loading2: false, isBindDisabled: false });
          } else {
            wx.hideLoading();
            page.setData({ isBindDisabled: false, loading2: false, btnHelp: true });
          }
          page.isShowValidateBtn();
        },
        fail: function (err) {
          page.isShowValidateBtn();
          wx.hideLoading();
          wx.showModal({
            title: '提示',
            content: "链接断开!",
            showCancel: false
          });
        }
      });
    } else {
      wx.getUserInfo({
        success: function (res) {
          var userInfo = res.userInfo;
          var paramData = {
            //授权信息
            nickName: userInfo.nickName,
            gender: userInfo.gender,
            city: userInfo.city,
            province: userInfo.province,
            country: userInfo.country,
            avatarUrl: userInfo.avatarUrl,

            //学生 绑定信息
            cellphone: page.data.cellphone.trim(),
            relationship: page.data.objectArray[page.data.index].value,
            notifyOrSMS: notifyOrSMS,
            firstName: page.data.firstName.trim(),
            lastName: page.data.lastName.trim(),
            dateOfBirth: page.data.date
          }
          api.bindStudent({
            method: 'POST',
            data: paramData,
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("openId"),
            },
            success: function (data) {
              if (data.data.code == 1) {
                if (data.data.data.status == 1) {
                  wx.showModal({
                    title: '提示',
                    content: '恭喜你，绑定成功。',
                    showCancel: false,
                    success: function (res) {
                      if (wx.reLaunch) {
                        wx.reLaunch({ url: '/pages/home/index' });
                      } else {
                        wx.switchTab({ url: '/pages/home/index' });
                      }
                    }
                  });
                } else if (data.data.data.status == 0) {
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false
                  });
                } else if (data.data.data.status == 2) {
                  wx.showModal({
                    title: '提示',
                    content: data.data.data.message,
                    showCancel: false
                  });
                }
                wx.hideLoading();
                page.setData({ loading2: false, isBindDisabled: false });
              } else {
                wx.hideLoading();
                page.setData({ isBindDisabled: false, loading2: false, btnHelp: true });
              }
              page.isShowValidateBtn();
            },
            fail: function (err) {
              page.isShowValidateBtn();
              wx.hideLoading();
              wx.showModal({
                title: '提示',
                content: "链接断开!",
                showCancel: false
              });
            }
          });
        },
        fail: function (err) {
          page.ReauthorizationByAuto(page, notifyOrSMS);
        }
      });
    }
  },
  ReauthorizationByAuto: function (page, notifyOrSMS) {
    wx.openSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          //授权用户
          wx.getUserInfo({
            success: function (res) {
              var userInfo = res.userInfo;
              var paramData = {
                //授权信息
                nickName: userInfo.nickName,
                gender: userInfo.gender,
                city: userInfo.city,
                province: userInfo.province,
                country: userInfo.country,
                avatarUrl: userInfo.avatarUrl,

                //学生 绑定信息
                cellphone: page.data.cellphone.trim(),
                relationship: page.data.objectArray[page.data.index].value,
                notifyOrSMS: notifyOrSMS,
                firstName: page.data.firstName.trim(),
                lastName: page.data.lastName.trim(),
                dateOfBirth: page.data.date
              }
              // if (notifyOrSMS == "SMS") {
              //   paramData.validateCode = page.data.validateCode
              // }
              api.bindStudent({
                method: 'POST',
                data: paramData,
                header: {
                  'Content-Type': 'application/json',
                  userAuth: wx.getStorageSync("openId"),
                },
                success: function (data) {
                  if (data.data.code == 1) {
                    if (data.data.data.status == 1) {
                      wx.showModal({
                        title: '提示',
                        content: '恭喜你，绑定成功。',
                        showCancel: false,
                        success: function (res) {
                          if (wx.reLaunch) {
                            wx.reLaunch({ url: '/pages/home/index' });
                          } else {
                            wx.switchTab({ url: '/pages/home/index' });
                          }
                        }
                      });
                    } else if (data.data.data.status == 0) {
                      wx.showModal({
                        title: '提示',
                        content: data.data.data.message,
                        showCancel: false
                      });
                    } else if (data.data.data.status == 2) {
                      wx.showModal({
                        title: '提示',
                        content: data.data.data.message,
                        showCancel: false
                      });
                    }
                    wx.hideLoading();
                    page.setData({ loading2: false, isBindDisabled: false });
                  } else {
                    wx.hideLoading();
                    page.setData({ isBindDisabled: false, loading2: false, btnHelp: true });
                  }
                },
                fail: function (err) {
                  page.setData({ isBindDisabled: false, loading2: false });
                  wx.hideLoading();
                  wx.showModal({
                    title: '提示',
                    content: "链接断开!",
                    showCancel: false
                  });
                }
              });
            }
          });
        } else {
          //未授权，提示
          wx.showModal({
            title: '提示：',
            content: '请授权才能使用。',
            showCancel: false,
            success: function (res) {
              page.ReauthorizationByAuto(page, notifyOrSMS);
            }
          })
        }
      }
    });
  },
  selectType: function(e){
    if (e.currentTarget.dataset.flag == "validate"){
      this.setData({ swiperCode: true});
    } else if (e.currentTarget.dataset.flag == "push") {
      this.setData({ swiperCode: false});
    }
  },
  makePhoneCall: function () {
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  }
})
