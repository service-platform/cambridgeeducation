// pages/ciieService/serviceDetails/serviceDetails.js
Page({
  data:{
    windowWidth: '0px'
  },
  onLoad: function(){
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          windowWidth: (res.windowWidth-30) + "px"
        });
      }
    })
  },
  makePhoneCall: function(){
    wx.makePhoneCall({
      phoneNumber: '400-600-8616'
    });
  }
})