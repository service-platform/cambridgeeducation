import API from '../../api/api';
import utils from '../../utils/util.js';
Page({
  data: {
    serverData: [],
    pageSize: 10,
    totalPage: 0,
    isLoading: true,
    isError: false
  },
  onLoad: function () {
    this.onRefreshData();
  },
  onPullDownRefresh: function () {
    this.onRefreshData();
    wx.stopPullDownRefresh();
  },
  onReachBottom: function () {
    if (this.data.totalPage > 0 && this.data.pageSize < this.data.totalPage * this.data.pageSize) {
      if (this.data.pageSize/10 < this.data.totalPage){
          this.data.pageSize += 10;
      }
      this.onRefreshData();
    }
  },
  onRefreshData: function () {
    var that = this;
    API.unBindStuList({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: {
        conditions: { },
        pageNumber: 1,
        pageSize: this.data.pageSize
      },
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isError: true });
          return;
        }
        if (json.data.code == 1) {
          if (that.data.pageSize <= 10) {
              that.setData({
                serverData: json.data.data.content,
                totalPage: json.data.data.totalPage,
                isLoading: false,
                isError: false
              });
          } else {
              that.setData({
                serverData: json.data.data.content,
                isLoading: false,
                isError: false
              });
          }
          if (json.data.data.total != 0){
              wx.setNavigationBarTitle({ title: '历史解绑记录（' + json.data.data.total + '）'});
          }
        }
      },
      fail: function (err) {
        that.setData({ isLoading: false, isError: true });
      }
    })
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})