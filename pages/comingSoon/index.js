//index.js
var app = getApp()
Page({
   onLoad: function(){
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          windowHeight: res.windowHeight + "px"
        });
      }
    })
  },
  gotoNavigatorUrl: function(e){
     wx.navigateBack();
  }
})
