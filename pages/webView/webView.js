Page({
  data: {
    webViewSrc: ''
  },
  onLoad: function (options) {
    this.setData({
      webViewSrc: decodeURIComponent(options.src)
    });
  }
})