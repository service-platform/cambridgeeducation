import api from '../../../api/api.js'
Page({
  data:{
    serverData:{avatarUrl: '/image/defaultHeader.jpg'},
    isUpdate: false,
    viewHeight: "0px"
  },
  updateHeaderImg: function(e){
    var page = this;
    this.data.serverData.avatarUrl = e.detail.userInfo.avatarUrl;
    wx.showLoading({ title: '更新中...', mask: true });

    api.updateParentProfile({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: this.data.serverData,
      success: function (data) {
        wx.hideLoading();
        if (data.data.code == 1) {
          page.refreshData();
          wx.showModal({
            title: '提示',
            content: '更新头像',
            showCancel: false
          });
        }
      },
      fail: function () {
        wx.hideLoading();
      }
    });
  },
  btnUpdateInfo: function(){
    this.setData({isUpdate: true});
  },
  formSubmit: function(e){
    //默认值
    if(e.detail.value.gender == ""){e.detail.value.gender = "1"}
    //验证电子邮件
    if(e.detail.value.email != ""){
        if(/^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$/.test(e.detail.value.email) == false){
          wx.showModal({
            title: '提示',
            content: '电子邮件，格式不对。',
            showCancel: false
          });
          return;
        }
        this.data.serverData.email = e.detail.value.email;
    }else{
      this.data.serverData.email = "";
    }
    this.data.serverData.nickName = e.detail.value.nickName;
    this.data.serverData.gender = e.detail.value.gender;
    this.data.serverData.country = e.detail.value.country;
    this.data.serverData.province = e.detail.value.province;
    this.data.serverData.city = e.detail.value.city;
    var page = this;
    wx.showLoading({ title: '保存中...', mask: true });
    api.updateParentProfile({
      method: 'POST',
      header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
      },
      data: this.data.serverData,
      success:function(data){
          wx.hideLoading();
          if(data.data.code == 1){
              page.setData({isUpdate: false});
              page.refreshData();
              wx.showModal({
                title: '提示',
                content: '个人信息，修改成功！',
                showCancel: false
              });
          }
      },
      fail: function(){
          wx.hideLoading();
      }
    });
  },
  formReset: function(){
    this.setData({isUpdate: false});
  },
  onLoad:function(options){
    this.refreshData();

    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          viewHeight: res.windowHeight +"px"
        });
      }
    })
  },
  refreshData: function(){
    var page = this;
    api.parentProfile({
      method: 'POST',
      header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
      },
      success:function(data){
        page.setData({
          serverData: data.data.data
        });
        var pages = getCurrentPages();
        var prevPage = pages[pages.length - 2];  //上一个页面
        prevPage.setData({
          serverData: data.data.data
        })
      }
    });
  }
})