import api from '../../api/api.js';
import utils from '../../utils/util.js';
Page({
  data:{
    serverData:{avatarUrl: '/image/defaultHeader.jpg'},
    noSeeCount: 0
  },
  onLoad: function(){
    var page = this;
    api.parentProfile({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      success: function (data) {
        page.setData({
          serverData: data.data.data
        });
      }
    });
  },
  onShow: function(){
    this.refreshData();
  },
  btnGuide: function(){
    wx.navigateTo({
      url: '/pages/skiingToEnjoy/skiingToEnjoy'
    });
  },
  btnUpdateInfo: function(){
    wx.navigateTo({
      url: '/pages/parentProfile/updateParentProfile/updateParentProfile'
    });
  },
  btnFeedbackList: function(){
    wx.navigateTo({
      url: '/pages/feedbacks/feedbackList'
    });
  },
  // btnAboutCIIE: function () {
  //   wx.navigateTo({
  //     url: '/pages/aboutCIIE/aboutCIIE'
  //   });
  // },
  btnMyFavorites: function(){
    wx.navigateTo({
      url: '/pages/articleList/articleList?title=收藏夹'
    });
  },
  btnHistoryUnBind: function(){
    wx.navigateTo({
      url: '/pages/unBindList/unBindList'
    });
  },
  gotoWebView: function(){
    wx.navigateTo({
      url: '/pages/webView/webView?src=' + encodeURIComponent("https://www.cambridgenetwork.cn")
    });
  },
  refreshData: function(){
    var page = this;
    //显示已评论状态下，未读反馈
    api.findNotCheckedFeedbacks({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      success: function (json) {
        if (json.data.code == 1){
          page.setData({
            noSeeCount: Number.MaxNum(json.data.data)
          });
        }
      }
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  },
})