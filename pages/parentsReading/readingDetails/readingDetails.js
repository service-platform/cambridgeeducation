//index.js
var app = getApp()
Page({
  data: {
    windowHeight: '0px',
    windowWidth: '0px',
    minWidth: '0px',
    minHeight: '0px',
  },
onLoad: function(){
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        var width = parseInt((res.windowWidth-30)*(3/5));
        var height = parseInt(width/0.57658);

        var minWidth = parseInt((res.windowWidth-30)*(2/5));
        var minHeight = parseInt(minWidth/0.57658);

        that.setData({
          windowHeight: height + "px",
          windowWidth: (width-5) + "px",
          minWidth: minWidth + 'px',
          minHeight: minHeight + 'px'
        });
      }
    })
  },
})
