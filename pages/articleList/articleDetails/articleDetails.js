import API from '../../../api/api';
import utils from '../../../utils/util.js';
var WxParse = require('../../../wxParse/wxParse.js');
Page({
  data:{
    id: "",
    title: "",
    likeCount: 0,
    shareNumbers: 0,

    commitTime: "",
    author: "",
    serverData: null,

    likeImageUrl: "/image/articleDetails/ThumbsUp.png",
    favoritesUrl: "/image/articleDetails/Favorites.png",
    scrollHeight: "0px",
    showViewComment: "none",
    showViewSignUp: "none",
    focus: false,
    txtContent: "",
    
    commentHeight: "0px",
    signUpHeight: "0px",
    txtWidth: "0px",
    pageIndex: 1,
    myOpenId: "",
    isFirstPlay: false,
    isAudioFirstPlay: false,
    hasComments: false,
    viewProgress: "0px",
    viewHeight: '0px',
    showWhetherBind: false,
    isShowSignUp: false,
    registrationData:{
      aId: "",
      pId: "",
      nickName: "加载中...",
      gender: "1",
      city: "",
      province: "",
      country: "",
      avatarUrl: "/image/noSex.png",
      phones: [],
      enrollphone: "加载中...",
      relationship: "加载中...",
      openId: "",
      sIds: []
    },
    relationshipStr: "",

    zoom: 1
  },
  setPlayCount: function(){
    if (this.data.isFirstPlay == false){
      this.data.isFirstPlay = true;
      //播放量 +1
      API.videoPlayCount({
        method: "POST",
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        id: this.data.serverData.video.id,
      });
    }
  },
  setAudioPlayCount: function () {
    if (this.data.isAudioFirstPlay == false) {
      this.data.isAudioFirstPlay = true;
      //播放量 +1
      API.audioPlayCount({
        method: "POST",
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        id: this.data.serverData.audio.id,
      });
    }
  },
  //解决 IOS 系统，提交无反应
  hideKeyboard: function(){
    wx.hideKeyboard();
  },
  onImgLoad: function (e) {
    this.data.serverData.imageTextList[e.currentTarget.id].bgPicture = "background-image:none";
    this.setData({
      serverData: this.data.serverData
    });
  },
  bindTimeUpdate: function(e){
    var currentTime = parseInt(e.detail.currentTime*100);
    var duration = parseInt(e.detail.duration * 100);

    this.setData({
      viewProgress: ((currentTime / duration)*304) + "px"
    });
  },
  bindThumbsUpComment: function(e){
    var that = this;

    var whether = "YES";
    if (e.currentTarget.dataset.thumbsup == "comment"){
      if (e.currentTarget.dataset.item.whetherCommentThumbsUp == true){
        whether = "NO";
      }else{
        whether = "YES";
      }
    } else if (e.currentTarget.dataset.thumbsup == "reply") {
      if (e.currentTarget.dataset.item.whetherReplyThumbsUp == true) {
        whether = "NO";
      } else {
        whether = "YES";
      }
    }
    
    API.articleThumbsUpComment({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      cId: e.currentTarget.dataset.item.id,
      thumbsUp: e.currentTarget.dataset.thumbsup,
      whether: whether,
      success: function (data) {
        if (data.data.code == 1) {
          for (var i = 0; i < that.data.serverData.partComments.length; i++) {
            if (data.data.data.id == that.data.serverData.partComments[i].id) {
              that.data.serverData.partComments[i].thumbsUp = data.data.data.thumbsUp;
              that.data.serverData.partComments[i].commentThumbsUpIds = data.data.data.commentThumbsUpIds;
              that.data.serverData.partComments[i].replyThumbsUpIds = data.data.data.replyThumbsUpIds;

              that.data.serverData.partComments[i].whetherCommentThumbsUp = data.data.data.whetherCommentThumbsUp;
              that.data.serverData.partComments[i].whetherReplyThumbsUp = data.data.data.whetherReplyThumbsUp;

              if (whether == "YES"){
                  wx.showToast({
                    title: '点赞 +1',
                    icon: 'success',
                    duration: 2000
                  });
              }else{
                  wx.showToast({
                    title: '取消点赞',
                    icon: 'success',
                    duration: 2000
                  });
              }
            }
          }
          that.setData({
            serverData: that.data.serverData
          });
        }
      }
    });
  },
  onReachBottom: function (e) {
    if (this.data.serverData.comments != undefined && this.data.serverData.comments.length > 5){
        var len = this.data.serverData.comments.length;
        var pageNum = Math.ceil(len / 5);

        if (this.data.pageIndex < pageNum) {
          this.data.pageIndex++;
          this.data.serverData.partComments = this.data.serverData.comments.slice(0, this.data.pageIndex * 5);

          this.setData({
            serverData: this.data.serverData,
            hasComments: true
          });
        }else{
          this.setData({hasComments: false});
        }
    } else {
      this.setData({ hasComments: false });
    }
  },
  btnDeleteComment: function(e){
    var that = this;
    wx.showActionSheet({
      itemList: ['删除评论'],
      itemColor: "#EC7676",
      success: function (res) {
        if (res.tapIndex == 0){
            that.deleteComment(e);
        }
      }
    })
  },
  deleteComment: function(e){
    var that = this;
    API.removeComment({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      cId: e.currentTarget.dataset.id,
      success: function (data) {
        if (data.data.data == 1) {
          that.refreshData();
          wx.showToast({
            title: '评论已删除',
            icon: 'success',
            duration: 2000
          });
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '提示：',
          content: '对不起，服务器连接错误，请联系相关管理人员。',
          showCancel: false
        })
      }
    });
  },
  bindFormSubmit: function(e){
    if (e.detail.value.content.trim() == ""){
      wx.showModal({
        title: '提示：',
        content: '评论内容不能为空。',
        showCancel: false
      })
      return;
    }
    wx.showToast({
      title: '发布评论中...',
      icon: 'loading',
      duration: 10000,
      mask: true
    });
    var that = this;
    API.comment({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      aId: that.data.serverData.article.id,
      data:{
        content: e.detail.value.content,
        anonymity: (e.detail.value.anonymity.length <= 0)?false:true
      },
      success: function (data) {
        if (data.data.code == 1) {
          that.setData({ showViewComment: "none", focus: false, txtContent: "" });
          that.refreshData();
          wx.showToast({
            title: '评论已发布',
            icon: 'success',
            duration: 2000
          });
        }
      },
      fail: function (res) {
        wx.hideToast();
        wx.showModal({
          title: '提示：',
          content: '对不起，服务器连接错误，请联系相关管理人员。',
          showCancel: false
        })
      }
    });
  },
  bindFormReset: function(){
    this.setData({ showViewComment: "none", focus: false });
  },
  showCommentView: function(){
    this.setData({ showViewComment: "block", focus: true });
  },
  onShareAppMessage: function () {
    var that = this;
    return {
      title: this.data.title + " - 详情",
      path: '/pages/articleList/articleDetails/articleDetails?id=' + this.data.id + "&likeCount=" + this.data.likeCount + "&title=" + this.data.title +"&isShare=true",
      success: function (res) {
        API.clickShare({
          method: "POST",
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId"),
          },
          aId: that.data.serverData.article.id,
          success: function (data) {
            if (data.data.code == 1) {
              that.setData({
                shareNumbers: (parseInt(that.data.shareNumbers) + 1)
              });
              wx.showToast({
                  title: '转载 +1',
                  icon: 'success',
                  duration: 2000
              });
            }
          }
        });
      },
      fail: function (res) {
        wx.showModal({
          title: '提示：',
          content: '转载文章失败，请重试。',
          showCancel: false
        });
      }
    }
  },
  openPreview: function (e) {
    if (this.data.serverData.imageTextList != undefined) {
      var imgArrays = new Array();
      for (var i = 0; i < this.data.serverData.imageTextList.length; i++) {
        if (this.data.serverData.imageTextList[i].pic != undefined) {
          imgArrays.push(this.data.serverData.imageTextList[i].pic);
        }
      }
      wx.previewImage({
        current: e.currentTarget.dataset.src, // 当前显示图片的http链接
        urls: imgArrays // 需要预览的图片http链接列表
      });
    }
  },
  setThumbsUp: function(){
      var that = this;
      if (this.data.likeImageUrl == "/image/articleDetails/ThumbsUp_Disabled.png"){
          API.thumbsUp({
            method: "POST",
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("openId"),
            },
            aId: that.data.serverData.article.id,
            success: function (data) {
              if (data.data.code == 1) {
                that.setData({
                    likeCount: (parseInt(that.data.likeCount) + 1),
                    likeImageUrl: "/image/articleDetails/ThumbsUp.png"
                });
                wx.showToast({
                  title: '点赞 +1',
                  icon: 'success',
                  duration: 2000
                });
              }
            }
          });
      }
  },
  setFavorites: function () {
    var that = this;
    if (this.data.favoritesUrl == "/image/articleDetails/Favorites.png") {
      API.weatherFollow({
        method: "POST",
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        aId: that.data.serverData.article.id,
        weatherFollow: "follow",
        success: function (data) {
          if (data.data.code == 1) {
            that.setData({
                favoritesUrl: "/image/articleDetails/Favorites_Enable.png"
            });
            wx.showToast({
                title: '收藏成功',
                icon: 'success',
                duration: 2000
            });
          }
        }
      });
    } else {
      API.weatherFollow({
        method: "POST",
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        aId: that.data.serverData.article.id,
        weatherFollow: "unFolllow",
        success: function (data) {
          if (data.data.code == 1) {
            that.setData({
                favoritesUrl: "/image/articleDetails/Favorites.png"
            });
            wx.showToast({
                title: '取消收藏',
                icon: 'success',
                duration: 2000
            });
          }
        }
      });
    }
  },
  //记住文章阅读位置
  onPageScroll: function(res){
    if (wx.pageScrollTo){
      var articleId = this.data.serverData.article.id;
      wx.setStorageSync("browseAddress", articleId + "_" + res.scrollTop);
    }
  },
  onLoad: function(param){
      var that = this;
      wx.setNavigationBarTitle({ title: param.title + " - 详情" });
      // 延迟更换标题，解决 异步回调 问题
      setTimeout(function(){
        wx.setNavigationBarTitle({ title: param.title + " - 详情" });
      },2000);

      this.setData({ id: param.id, likeCount: ((param.likeCount == undefined)?0:param.likeCount), title: param.title, myOpenId: wx.getStorageSync("decryptedOpenId") });

      wx.getNetworkType({
        success: function (obj) {
          if (obj.networkType == "none") {
            that.setData({ showWhetherBind: true });
          }else{
            wx.showLoading({ title: '加载中...', mask: true });
            wx.login({
              success: function (res) {
                if (res.code) {
                  API.whetherBind({
                    code: res.code,
                    success: function (data) {
                      if (data.statusCode == 200) {
                        if (data.data.code == 0) {
                          wx.setStorageSync("openId", data.data.data);
                          wx.redirectTo({
                            url: '/pages/binding/binding?flap=true',
                          });
                        } else {
                          wx.setStorageSync("openId", data.data.data);
                          that.refreshData();
                        }
                      } else {
                        that.setData({ showWhetherBind: true }); wx.hideLoading();
                      }
                    },
                    fail: function (err) {
                      that.setData({ showWhetherBind: true }); wx.hideLoading();
                    }
                  });
                }
              },
              fail: function (err) {
                that.setData({ showWhetherBind: true }); wx.hideLoading();
              }
            });
          }
        },
        fail: function (err) {
          that.setData({ showWhetherBind: true }); wx.hideLoading();
        }
      });
      
      wx.getSystemInfo({
        success: function (res) {
          that.setData({
            scrollHeight: (res.windowHeight - 50) + "px",
            commentHeight: (res.windowHeight - 11) + "px",
            signUpHeight: (res.windowHeight) + "px",
            txtWidth: (res.windowWidth - 32) + "px",
            viewHeight: res.windowHeight + "px"
          });
        }
      });
      wx.onNetworkStatusChange((paramData)=>{
        if (paramData.isConnected == false){
          // that.setData({ showWhetherBind: true });
        }else{
          that.bindWhetherBindRefreshData();
        }
      });
  },
  bindWhetherBindRefreshData: function(){
    var that = this;
    that.setData({ showWhetherBind: false });
    wx.getNetworkType({
      success: function (obj) {
        if (obj.networkType == "none") {
          that.setData({ showWhetherBind: true });
        }else{
          wx.showLoading({ title: '加载中...', mask: true });
          wx.login({
            success: function (res) {
              if (res.code) {
                API.whetherBind({
                  code: res.code,
                  success: function (data) {
                    if (data.statusCode == 200) {
                      if (data.data.code == 0) {
                        wx.setStorageSync("openId", data.data.data);
                        wx.redirectTo({
                          url: '/pages/binding/binding?flap=true',
                        });
                      } else {
                        wx.setStorageSync("openId", data.data.data);
                        that.refreshData();
                      }
                    } else {
                      that.setData({ showWhetherBind: true }); wx.hideLoading();
                    }
                  },
                  fail: function (err) {
                    that.setData({ showWhetherBind: true }); wx.hideLoading();
                  }
                });
              }
            },
            fail: function (err) {
              that.setData({ showWhetherBind: true }); wx.hideLoading();
            }
          });
        }
      },
      fail: function (err) {
        that.setData({ showWhetherBind: true }); wx.hideLoading();
      }
    });
  },
  refreshData: function(){
      var that = this;
      wx.showNavigationBarLoading();
      API.findArticleDetial({
        method: "POST",
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("openId"),
        },
        id: this.data.id,
        success: function (data) {
          wx.hideNavigationBarLoading(); wx.hideLoading();
          if (data.data.code == 1) {
            if (data.data.data.imageTextList != undefined) {
              for (var i = 0; i < data.data.data.imageTextList.length; i++) {
                WxParse.wxParse('article_' + i, 'html', data.data.data.imageTextList[i].content, that);
                data.data.data.imageTextList[i].nodes = that.data["article_" + i].nodes;
              }
            }
            if (data.data.data.video != undefined) {
              WxParse.wxParse('videoContent', 'html', data.data.data.video.content, that);
            }
            if (data.data.data.comments != undefined) {
              for (var i = 0; i < data.data.data.comments.length; i++) {
                data.data.data.comments[i].commentDate = new Date(data.data.data.comments[i].commentDate).Format("MM-dd hh:mm");
                data.data.data.comments[i].openIdThumbnail = data.data.data.comments[i].openId.substr(data.data.data.comments[i].openId.length - 6) + "....";
                if (data.data.data.comments[i].replyDate != undefined){
                  data.data.data.comments[i].replyDate = new Date(data.data.data.comments[i].replyDate).Format("MM-dd hh:mm");
                }
              }
            }
            data.data.data.partComments = data.data.data.comments.slice(0, that.data.pageIndex * 5);
            if (data.data.data.audio != undefined){
              if (data.data.data.audio.thumbnailUrl == undefined || data.data.data.audio.thumbnailUrl == "") {
                data.data.data.audio.thumbnailUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAgAElEQVR4Xu2dCZwdRbX/z+l7k0kCCfsTkD0gW2Ryq+qGMARwEHFDUZDt8UDRKKCIiooLLiCoKJu4oEBcABcIoPwF9PkB/gFfGJPcrr4ZiIAvhi0KCGHJDCSZzEyf9ym9YAJZ+i5dXdV96vOZT8KHqlPnfE/NL33rVp9C4MYEmAAT8IQAeuInu8kEmAATABYsXgRMgAl4Q4AFy5tUsaNMgAmwYPEaYAJMwBsCLFjepIodZQJMgAWL1wATYALeEGDB8iZV7CgTYAIsWLwGmAAT8IYAC5Y3qWJHmQATYMHiNcAEmIA3BFiwvEkVO8oEmAALFq8BJsAEvCHAguVNqthRJsAEWLB4DTABJuANARYsb1LFjjIBJsCCxWuACTABbwiwYHmTKnaUCTABFixeA0yACXhDgAXLm1Sxo0yACbBg8RpgAkzAGwIsWN6kih1lAkyABYvXABNgAt4QYMHyJlXsKBNgAixYvAaYABPwhgALljepsu9oX1/f+AkTJuwWx/F2RPQ6ItoaAF75QcQtAWAiEU0AgAlBEExo/H1iw9tBRFwRx/EKAFhh/g4Ag0T0HAAsC4LgmTiOlzX+/jQAPLF8+fKHe3t7V9mPlmf0gQALlg9ZStHHhQsXvn54eHgfRNwbAHYHgJ0AYGdE3BkAtkhx6g2ZfhYAHgeAxxo/f0XEB+M4fkAp9WRGPvG0DhBgwXIgCbZcqNfrU0dHRw9GxAoAGIEyP5Nszd+heV4AgAeJ6AFErMdxfE+1Wl3UIdtsxnECLFiOJ6hV94go0FpPRcRDAOBNAHBQhk9MrYaRdJz5WHkPIt4Tx/HdSqn7kw7kfn4RYMHyK1/r9dYIVBiG3UEQGHHqBYCDAWCznITXbBjmI+XdADAHEecIIR5o1gD3d5MAC5abeUnkVV9f35Zjx449DgDe2niS2jzRwOJ1eqYhYL9fuXLlTTNmzBgsHoJ8RMyC5Vke586dO3H8+PFHA8DxAPBmACh7FkLW7g4BwO8B4FdDQ0O39vT0rMzaIZ4/OQEWrOSsMu2ptT4WAE4EgHdn6ki+JjfHLH4DANdJKf+Qr9DyGQ0LlsN5jaLIHC04jYg+AADbOuxqHlx7FABmlcvlq7u7u82ZMG4OEmDBcjApYRgeg4inNzbPHfQw9y7dhohXCCHMR0duDhFgwXIkGfPmzZtULpc/gohnAsCOjrhVdDceIqJLxo0bd+2UKVNWFx2GC/GzYGWchf7+/h2Gh4c/hYgfNq+5ZOwOT79uAk8T0ffHjh37/f322+95hpQdARasjNhrrXcnoi8j4skZucDTNk9gFSJeVSqVvs77XM3D68QIFqxOUGzCRhiGOyHiuQBwEh9JaAKcW13Nt4s/GBoaurCnp8e8yM3NEgEWLEugzUvGo6OjXwGAUwBgjKVpeZp0CbwIAJcHQXBxpVIx7zhyS5kAC1bKgPv7+zcZHR09h4jOAoCulKdj89kQeJ6Izh0cHLyit7d3JBsXijErC1ZKeSYirNfrpxDR1/kMVUqQ3TP7ECKexcch0ksMC1YKbGu12oFBEPwAALpTMM8m3SdwVxAEp1cqlcXuu+qXhyxYHczXfffdt8Xq1au/w9/8dRCqv6aGiehb48aNO5/PcHUuiSxYHWIZRdEJRHQ5AGzTIZNsJh8EzFPWyVLKefkIJ9soWLDa5G8Ofo6MjPwYAA5v0xQPzy8BQsQfrVix4nNc2qa9JLNgtcEviiLzYvJFALBpG2Z4aHEI/B0R3y+EuKs4IXc2UhasFnjW6/XN4zi+FgDe1cJwHlJsAgQAFxPROUqp4WKjaD56FqwmmUVRdDAR3cBHFZoEx91fTaCfiI5WSi1hNMkJsGAlZwVa628DwGebGMJdmcCGCJhqp5+UUl7FmJIRYMFKwCmKom2I6LcAMD1Bd+7CBJolcF1XV9dMPv6wcWwsWBthpLWWAHArAGy3cZzcgwm0TECXy+X3dHd3/61lCwUYyIK1gSRrrU8FgO8CwNgCrAUOMXsC5nqy90op/yd7V9z0gAVrPXkJw3AWIn7IzbSxV3kmQEQfU0pdkecYW42NBetV5BrXaP0aAA5rFSqPYwIdIHCxlJK/4HkVSBasNYAsWLBg2yAI7kTEfTuw4NgEE2iXwI2TJk06aY899jB3KXIDABasxjKo1Wp7IqIRqx14ZTABhwj8iYjerpRa7pBPmbnCggUAYRjOQMTbAWBSZpngiZnA+gk8QESHKaWeLDqkwguW1voIALiJq4EW/VfB+fiXAsChUsq/Ou9pig4WWrCiKDqeiH4BAEGKjNk0E+gUgWfjOH5LtVqtd8qgb3YKK1ha6zMaZ6wKy8C3xcr+/pPAS3Ecv7Vard5bRB6F/GUNw/AriHheERPOMeeGwNullP+dm2gSBlI4wQrD8BOI+J2EfLgbE3CVwKo4jg+pVqsLXHUwDb8KJViNMsa/TAMk22QCGRB4IY7jg6rV6qIM5s5kysIIVuPbwFsAoJQJaZ6UCaRAABH/EcfxNKXU4ymYd85kIQSrXq/3xHE8h19idm79sUOdIbBkZGRk//3339+8PJ3rlnvBCsNwL0Scz4dCc72OOTiAcGho6OCenh5TFDC3LdeCNW/evNeNGTNGA8Drc5tBDowJ/JvAbUKIIxExziuU3ApWf3//JiMjI38CgDfmNXkcFxN4NQFE/KEQ4qN5JZNLwZo9e3Zp8uTJ5owKl4jJ68rluNZLgIg+q5S6OI+IcilYURRdTUQz85gwjokJJCFARO9VSplvxXPVcidYYRjORMSrc5UlDoYJNE/gRQCo5O1l6VwJVhiGAhHnAcCY5vPLI5hA7gg8SERKKbUiL5HlRrD6+vq27Orquh8Ats9LcjgOJtAuASK6SSl1TLt2XBmfC8EioiCKInMw9GBXwLIfTMAVAoj4aSHEpa74044fuRAsrfW5APDVdkDwWCaQZwKI2COEMMd8vG7eC1ajvPEfuT691+uQnU+fwNKVK1fuO2PGjMH0p0pvBq8FKwzDrRHR7Fttmx4itswEckPgZinl+3yOxmvB0lrfCQBv9jkB7DsTsEzgNCnllZbn7Nh03gpWFEVnEdElHSPBhphAMQisKpVK3VOnTv1fH8P1UrD6+/t3HRkZeZBvuvFxybHPDhAIhRDTEJEc8KUpF7wULHOEgYje1FSk3JkJMIFXCCDimUKI7/mGxDvBiqLoZCK6xjfQ7C8TcIzA4Ojo6BumTZv2lGN+bdAdrwSrXq9vHsexuUhyK58gs69MwFECN0opj3XUt3W65ZVghWE4CxE/5BNg9pUJuEwAEd8hhPi9yz6u6Zs3ghWG4f6NF5t9Yct+MgHnCRDRI0qp3Zx3tOGgT4JVR8SpvoBlP5mARwQ+J6X8tg/+eiFYvNHuw1JiHz0mMICIuwshnnE9BucFKwzDCYj4KABs4zpM9o8J+EoAEWcJIT7suv/OC5bW+gIAOMd1kOwfE/CcACHiFCHEAy7H4bRg9ff37zAyMrLUZYDsGxPIEYE5UspDXY7HacHSWv8EAE5xGSD7xgTyRCCO48Or1eodrsbkrGDVarU9gyAwj6eBq/DYLyaQQwL9Ukpnv413VrDCMLwREb2u3ZPDxbyxkJ4FgOfMDyKOENEkANgEADZt/EzYmAH+/04QOEpK+RsnPHmVE04KVuP2G3PFPDe3CJivve8FgJCIakT0TKlUem716tXPT58+fSCJq2EYTg6CYNc4jvcAgN0BYDIi7s9FGJPQs9bnASnlvtZma2IiJwVLa21ubX5rE3Fw13QIzEHE+QDwpyAI9NSpU/+ezjQA9Xp9KhG91fwAQG9a87DdZAQQ8WQhxHXJetvr5Zxg8dOVveSvZ6bbzNVQAHCLUmp5Ft709/dvMjw8/BZEPBoAjgIA/ihpPxEPSin3sT/thmd0TrC01tcDwHGugcqxPy8BwO8A4OZyuXxbd3e3+W9n2uLFi7sGBgbeCQCmqsARjT0xZ/zLsyNEdIRS6naXYnRKsOr1+i5xHC/hbwatLBGzQX5puVy+3DWRWl/0c+fOnTh+/PjTAOBTALCdFUoFngQR7xZCOPXx3CnB0lqbCohnFHiN2AjdbJxfXC6Xf+CLUL0aSuOp62QAOLuxcW+DW1Hn6JZS3udK8M4I1n333bfF8PCw2dQd7wqcnPnxFBFdvHr16it6enpW5iG22bNnl3bbbbdTgyA4n4i2zENMDsZwg5TyeFf8ckawwjD8PCJ+0xUwefKDiL4yODh4UW9v76o8xfVyLH19fVt2dXV9DQA+lsf4so4JEV8vhHgiaz/M/M4Iltb6MQDYyQUoOfKhv1QqnTh16tQ/5yim9YaycOHCfeM4vpKIDixCvLZiRMQvCyFMEYLMmxOCFYZhLyL+/8xp5MeBYSK6YHBw8Bu9vb0j+QkrWSRhGJ6OiJcCwLhkI7jXhgiYqqRSSnPAN/NrwZwQrCiKfkFE/8nLpiMECvVUtT5i5mlrdHT0ZgDYsyNUC24EEQ8TQtyVNYbMBWvevHmTxowZ8zRfitr+UiCiawFgplJquH1r/luYM2fOuEmTJn0XAJwvTOc6bSL6lVIq84eKzAUriqKPE5FZVNxaJ2Ae1b8kpfxG6ybyOzKKog8S0SyX9mw9pD0cBMF/VCqVF7L0PXPB0lrXAEBlCcHzuVcDwAlSyl97Hkeq7tfr9SPjOL6Bn+TbwnyalPLKtiy0OThTwVqwYMGOpVLp8TZjKPLwZY3XJ8wLytw2QiCKogOIyNzBtxnDaolA5hVJMxUsrbWp1e7E16UtpS/DQUT0vwDwFqUUC34TeWgUhryby9k0Ae3fXWl0dHT7LK+3z1qw+gFgv5bQFXvQEiKarpRaVmwMrUXfeLI3db12bM1CcUcR0ceUUldkRSAzwdJam+Jti7MK3ON5nyUiwU9W7WWw8aQ1DwA2b89S4UbPlVIelFXUmQlWFEXnmVdGsgrc03nNO4AHSSm5GmsHEhhFkSKie7jeVnMwS6XSDmkWc9yQN5kJltba/NKJ5lAVuncMAO+UUppqrNw6REBr/TYAuA0ASh0yWQQzH5FSXp1FoJkIVhRF2xCROSzKLSEBRJwphPhxwu7crQkCWutPAsBlTQwpetebpZSZXBCTlWCd1DiVXfTEJ4qfiL6llPp8os7cqSUCYRjegohHtjS4eINeEkJMzOLdwqwEi98dTLjIEfFeIcSMhN25W4sEGtVMTaG6XVo0UahhcRzPqFar5ptWq826YBERRlH0PB/eS5TnVYi4lxDClN7hljKBKIq6zfVlADAm5am8N4+I5wshrH9pZl2wtNbS3GvnfcYsBICIZwohTNlobpYIaK1NEcDvW5rO52nmSSkPsB2AdcEKw/AziHiR7UA9nK8mhNg/i30CD1l11GWttTnqcHBHjebPWDw0NLSp7XLbWQgWb25ufPGuIqIpSilzgxA3ywSiKNqHiMx+Fh912AD7LGpkWRcsrbW5tWVry2vQq+mI6DNKqUu8cjpnzmqtvwMAn8hZWB0Nh4i+qpQytfStNauCFYbhXoj4oLXo/Jzoya6url2mTJliysZwy4hA41tD84S7TUYuOD8tIt4hhDjcpqO2BWsmImZyQtYm1Hbmyvrl0nZ8z9tYrbW5sNXUhue2bgIDQogtENG8hWGlWRUsrfVPAeADViLzc5InpJSv99P1/HkdhuEERDTle7bKX3SdiQgRpwohTNUVK822YJmNzDdaiczPST4upeSv1B3KHdds22gyPiilNA8iVpo1wSKiIIoiczlCYCUy/ybhvSsHc9bYyzI3kk900D0XXLpMSnmWLUesCVatVpsSBMH9tgLzbR4i+oRSii/jcDBxWuuvA8AXHXQtc5dsb7xbE6woik4gol9mTthRB7q6uiZOmTLlRUfdK7RbYRhujYjmOA631xJ4Skq5nS0wNgXrm0TEFQfWndnrpZQn2Eo6z9M8gTAMb0TETEqqNO+t3RFDQ0Nb9fT0PGdjVpuCdTsRvcNGUB7O8W4p5a0e+l0Yl8MwfA8i/qYwATcRKBEdqpSa08SQlrtaEyyt9V8BYHLLnuZ34PNE9Dq+rdntBIdhOAYR/wEAW7jtaSbeWbuv0KZgjfC7WetcTFdLKT+SyTLjSZsioLW+iq+9fy0yRLxQCPGFpmC22NmKYPGFqevPThzHvdVq1dyTx81xAlEUHdy4tMJxT627Z20P1opgcaLXu4CeFkJsyyVkrP+CtTRh4yzhC3wm6zX4rNXGsiVYJxPRNS2tknwP+pmU8pR8h5iv6LTWvwWAd+UrqrajsXa0wYpgaa2/CgDnto0lfwbOkFL+IH9h5TeiKIrOIiIu/bN2imlgYGBsb2+v2adOtdkSLN6sXEcagyCYVqlUTA3xXLRarXYgIu4MANs3AnoiCIJHhBB/ykWAAFCv16fGcVzPSzydiqNcLu/W3d39SKfsrc+OLcEy51fek3YwvtkfGBgY39vbu8o3v9f0t1ar7VYqlb5AREdv4Cv/ZwHgZkT8Rh4u1NBaD/A+1qsesYimK6Xmp72WbQnW/wAAX1W1djYjKaW5kMPbFobhhYj4uWYCQMSvCSHMFoG3LYqiOUT0Jm8DSMFxRHyHEOL3KZhey6QtwXoIAPZMOxif7CPiLCHEh33y+WVf+/v7dxgZGTHXu3e36H9fuVx+b3d3t5e3f0dRdDURzWwx9lwOQ8T3CyGuTTs4W4LFddxfm8lTpZRmb8+rprXeHQD6OlA6+LHR0dHp06ZNe8orAOaOOr75aV0p+5SU0tTBT7XZEixTB6ucaiSeGc/ixpF2ETWqFpg7Jc3GeifafeVyuae7u/ulThizZaNerx8Zx/EttubzZJ6vSym/lLavtgSL0g7EN/txHItqterVt01hGF6DiCd3kjUiXiKE+EwnbaZtS2u9NwA8kPY8PtlHxB8KIT6ats+2BMsUqbcyV9rAOmUfEXfx6Rsz821gEASLU6gYu5KIdlJKLesU27TthGG4GSKaE+/c/k3Ayus5VkREa81PWK9a2itXrpw0Y8aMQV9WvNb6SgBI5SVtH7855DX9mpX7eyll6uWjbAkWV2pYO78kpfSmtv3s2bNLu++++9NEtGVKAvuglHKflGynYlZrbf6x2TQV434atfI+oS3B4iestRfh01LK1/myLsMwnIGI5ixdao2IdlZKmSu1vGhaa3Mxxcsn+r3wOWUnrfyjY0uwhgBgbMrAfDL/kJTSbNx60bTW/wUA16XpLBEdoZS6Pc05Omlba21uMN+rkzY9t2XlBWhbgmVeP+nyPCGddL9PSnlgJw2macvGuSPfbg2Komg+EU1Lk7tntldJKcen7bMtweI9rLUzOV9KOT3t5HbKfhiGn0bEiztlb112EPHTQghvroXXWt8LAD1pMvHM9qiUMvWzlrYEawUApK6+HiX4L1JKbz5OWLqi7Xgp5Q2+5FBrzbeYr52sFVLKTdLOny3BMmdWNks7GF/sm8sMTKVRX/zVWpunwVRLxCBiVQhhTtF70bTWppTKLl44a8fJ56WUaX2L/EoEtgTLvOS6jR1uXswSSylLXngKAESEURSZ90G3Ssnnp4QQ2/tUKlprbQ66psUjJczpmbX1j7AVwQrDcCki7pAeLv8sSymtsO8UmTQPjhLR95RSZ3bKVxt2+ODoayg/JqVM/YnTyi9NGIYPI+KuNhaSL3MQ0fZKqSd98bdRpeEvKbyaMzo6OrrrtGnTlvrCYtGiRWOHhobMUR1u/yawWEr5hrSBWBEsrfWfAcCrk8xpgyeivZVSpk6YN01r/XMAOLHDDl8lpTy1wzZTNVev13eJ4zj1csCpBtF54/dJKVutj5bYGyuCFUXRXCLy5txRYnptdLRVobENF18ztFFextSg79Sj/0Njxozp2W+//Z7vpJ9p29Javw0AUq+umXYcHbZ/j5Qy9SqsVgQrDMNbEPHIDgPy3dw5Uspv+BZEvV7fg4jmdeC9QnNBxYGVSuVR3xiEYXgmIl7um99p+ktENymljklzDmPbimBprX8CAHz/3hrZtJXgNBZQ4yPRrQAwpUX7moje5dMe3ppxhmH4Q0Q8rcXY8zrsSill6kxsCda3AeCzec1Ui3FZ2aRs0beNDuvv799kZGTkAgD45EY7r9HBx1Iyr45Pa30XABzaTNx570tEFyilvpx2nFYEK4qizxHRhWkH45v9rq6uiVOmTHnRN79f9bSxVxAEnyei9wLApPXEkrdrvpZvIFaf09my70T0SaVU6h+TbQnWh4hoVss0cjqwsYdjLnTIRYui6BAi2oGItmsE9AQiPiylnJeLAAGgVqtVgiCI8hJPp+IgohOVUr/slL312bEiWPytyrrxI+KZQojvpZ1ktt85AjYqV3TOW3uWEPFNQoh70p7RimCFYbgXIpr6QdzWJvBTKeUHGYo/BKIoup2IUi8F7A+Rf3lqqwCjFcHq6+sb39XVZSo2cFubwFIhxM4+vUNX5AQ23qk0pZFTr0rgGWcrpWUMEyuCZSaKougpIvKmLLDFBXOwlDLV8sMWY8n1VFEUHUZEd+Q6yNaCM/uUk1sb2twoa4KltTYbr/s3517+e9u6zy3/JNOPkM8TrpfxXVLKw9LPgMUnLK319QBwnI2gPJvj+SVLlmxz7LHHjnrmd6HcDcNwDCI+xzflvDbtRPRjpdRMGwvC2hNWFEXnEdFXbATl2xxxHL+zWq3+zje/i+Sv1vooALi5SDE3EevZUsqLmujfclebgvU+IrqxZU/zPfDnUsqT8h2i39FprW8CgKP9jiI1798mpfxDatbXMGxNsGq12p5BEHhVTsVGAl6eY3h4eNvp06f/w+acPFcyAn19fVt2dXWZ0/rc1kHAZm03a4JFREEURSv5fsJ1r3kiulQp9Wn+jXCPgNb6XAD4qnueOeHRC1LKLWx5Yk2wTEBaa/NKQ8VWcJ7Ns5KIdlJKmVrh3Bwh0DhD+AQAbO6IS065gYh3CyF6bTllVbDCMLwGEU+2FZyH81wkpTzbQ79z63IURWcR0SW5DbDNwGzX47cqWFprU4rksjYZ5Xn4isYrDvyU5UiWtdZ/B4DtHXHHRTdOkVL+zJZjVgWrXq9X4zheYCs4H+dBxAuFEF/w0fe8+ay1/jAAXJW3uDoZTxAEb6hUKos7aXNDtqwKVuPwnaklxLdArz8rLwZBsGOlUjGXz3LLiEBj7+phAPDmwtsMUD0rpdza5rxWBcsEFkXRHCJKvVi9TYgpzPUTKeWHUrDLJhMSCMPwEkQ8K2H3onb7tZTS6tk064IVhuH5iPilomY4adyIeJgQwpTi5WaZQK1WmxIEwUIA8OZ2bsuIXp7uLCml1T1p64LFxfwSL62/d3V17eV7CeXE0TrUUWsdAoB0yCUnXQmCYFqlUjHXvllr1gVr0aJFmw4NDZmaQtw2QgARZwkhzMYvN0sEuKJoYtCDUsr11fBPbKTZjtYFyziotf4jABzUrLNF7M8fDe1lvV6vT43j2DwxlO3N6u1Mv5FSmhfCrbasBOscADBXRHHbOAH+aLhxRm33mDt37sTx48ff18Fbrdv2yXEDp0oprR/5yESwoihSRGT1s6/jyd+Ye7cJIY5ExHhjHfn/t0ZAa/1rADBXlXFLQKBcLu/Y3d39twRdO9olE8Fq1MY2xdD4/ayE6eTKpAlBtdAtDMOZiHh1C0OLOuRBKeU+WQSfiWCZQKMo+gUR/WcWQXs8p/WvkT1mlcj1er3eE8fxHK4ikgjXy50uk1JmckYtS8E6gYhSv3ixqTT40fkoKeVv/HDVbS/DMBSIaO7S29RtT93yjogOVUoZkbfeMhOsxqsPpigav6bTXNpXIuLBQghzVohbiwQWLlz4htHRUXPr9lYtmijkMET8R6VS2S6rq+kyEyyT7TAMb0TE9xUy8+0FvSwIgmqlUnm0PTPFHL1gwYIdgyDoQ8Qdikmg9agR8btCiE+0bqG9kVkL1jGIOLu9EAo7+u+IeLgQ4oHCEmghcFOqGxHvZLFqAR4AxHE8o1qt3tva6PZHZSpY/LGw7QS+SETvzmo/oW3vLRtoHKe5EwA2szx1XqZbKqXcKctgMhUsE7jW+lcAcHyWEDyf29xneLqUkr+W30Aioyh6OxGZs1bjPM93lu5fLKX8bJYOZC5YtVrtHUEQ3J4lhJzMfbEQ4uysNkNdZdg48/dFADiPqy+0l6VSqTRl6tSpf27PSnujMxesxm06S7kMbXuJbIz+4+jo6HHTpk17qiPWPDfSuJ7rBgCwco2657g26D4iLhBC7J91jJkLlgHANbI6ugxMpdKTpZS3dtSqZ8Zqtdq0IAjMR8DXe+a6q+5+xIVtBycEq7+/f9eRkZElAOCEP66umCb9+llXV9fHi1hPq3HTzYUAMKZJZtx93QTM5SjbKKVWZA3IGYHQWptvb96cNZCczf9YEATH2C6ylhXDhQsX7hvH8ZVEdGBWPuR0XmdKdrskWMcBwPU5TXjWYZ0npTS3F+eymb2qcePGfYuIZuYywIyDIqLpSqn5Gbvxz+mdEaw5c+aUJ02aZE5u855DOivjSSL61urVq6/q6elZmc4Udq3Onj27NHny5NMR8Twi2tLu7IWZLZRSVl2J1hnBMkC4PG36y8K8C2aEa2ho6Ee+CteiRYvGDg0NnQIAnwGA3dOnVtwZzKtzQoibXSHglGA1qj6am3YnugIox348TUTfBoAfurCZmoRz4z6AjwLAp/i+wCTE2u7zqBBiskuFI50SLINXa31R41/OtmmzgUQEBojoliAIrl++fPkdvb29I4lGWeq0ePHirsHBwSOI6BgAOAIANrE0deGnQcTThRA/cgmEc4IVhqEpXfGES5AK5MsAIppaWz8XQphvbTNp/f39mwwPD78FEU9oiNSETBwp9qTLBgYGduzt7V3lEgbnBKuxlzULEfnm42xXyksAsAAR5xFRWC6XF6RZwzuKom4iOhwR38E3g2ebeDM7EX1BKWXOsjnVnBQsU6+oVCo97hQpdsnwYvMAAAqwSURBVMYQeIaITB2pmrlEhIieKZVKz61evfr56dOnDyRBFIbh5CAIdo3jeI/GhrnZI+kBgG2SjOc+VggsGxoa2snFL2WcFCyTkiiKLieiM62khyfpFAFTQdZcLmL+pEbpYbPnZEoQmx/+aNcp0unaOUNK+YN0p2jNurOCNX/+/K3K5bK5RojLgbSWWx7FBJomQETmd243pdRw04MtDHBWsBpPWd8wn6UtcOApmAATMCfJEd8vhLjWVRhOC1a9Xt88juNH+P5CV5cP+5UzApndN5iUo9OC1XjK+jgRfTdpQNyPCTCB1ggg4iFCiD+2NtrOKOcFq/G+2EP8CoadBcGzFJbAb6WUR7oevfOC1XjKMvW4f+c6TPaPCXhKwGyw7yOl/Kvr/nshWAai1voPAHC460DZPybgIYHML5dIyswbwQrDcC9EfDBpYNyPCTCBRASWdXV17epLZVpvBKvxlPUtADg7URq4ExNgAkkInCSl/HmSji708UqwGhev/gUAdnQBHvvABHwmgIh3CyF6fYrBK8HiDXiflhb76jiBoXK5vHd3d7c55+hN806wGh8NTQXEo7yhzI4yAccIENFXlVJfc8ytjbrjpWAtWLBg21Kp9DAAjN9ohNyBCTCBtQgQ0SODg4NvcK1YY5I0eSlYjY+GHySiHycJkvswASbwCoGROI4PrFarC3xk4q1gNT4a3gQAR/sInn1mAhkR+JyU0tTy97J5LVjm0opx48b1I+KuXtJnp5mAXQJ3CSFM6WlTq8zL5rVgGeK1Wq0SBIF5vC17mQF2mgnYIbCMiPZWSi2zM106s3gvWI2Php8FAG8fc9NJLVtlAv8mgIiHCSHu8p1JLgSrIVq/BYB3+Z4Q9p8JdJoAEX1FKXV+p+1mYS83gtW4ZLPOZWiyWEY8p8ME7hRCmNuIvN23WpNtbgSr8ZS1t7nxni87cPjXh12zSeBxItpPKbXc5qRpzpUrwTKgoih6HxHdmCY0ts0EPCAwRERVpdT9Hvia2MXcCVZDtPiKsMRLgDvmkYC5NVsIcX3eYsulYBFREEXR/2tcc563nHE8TGCDBBDxy0KIC/KIKZeCZRLVKEVjCuqrPCaOY2IC6yKAiNcIIT6QVzq5FSyTsDAMtzbXqgPALnlNIMfFBNYgcOeSJUveduyxx47mlUquBcskrV6v7xHHsfnmcFJek8hxMQEAuL9cLh/Q3d39Up5p5F6wGk9aMxDxDr72Ps9LudCxLS2Xy6q7u/vpvFMohGCZJGqt3wYAtwFAKe9J5fgKReDpOI4PqFarpj5c7lthBKshWv8FANflPqscYFEIDJZKpQOmTp3656IEXCjBMkmNougsIrqkKAnmOHNLYFUQBG+uVCp9uY1wHYEVTrAaonWeeSG0SInmWPNFII7jd1ar1cLdhl5IwWp8PDwDAL6Xr2XM0RSAwGAcx2+vVqv3FiDW14RYWMEyJMIwfD8i/gQAgiImn2P2jsCzRNSbt/cDm8lCoQWr8aR1BACY2vBdzYDjvkzAMoGlAHColPKvlud1arrCC1ZjT+sQIjJHHjZ1KjvsDBP4F4FFRHS4UurJogNhwWqsgIULF+47Ojp6JwBsW/RFwfE7ReCelStXvmvGjBmDTnmVkTMsWGuA7+/v32F4ePi/EXHfjPLB0zKBNQncSEQnKqWGGcu/CLBgvWolmKvDxo8ffysAHMKLhAlkRQARvymE+GJW87s6LwvWejKjtTYn4s3JeG5MwCoBIvqwUmqW1Uk9mYwFawOJiqLo40R0Kd956Mlq9t9Ns6l+lJRynv+hpBMBC9ZGuGqtDwKAmwFgm3RSwFaZwD8JzEPEdwshnmEe6yfAgpVgdYRhuB0imn0tmaA7d2ECzRK4YmBg4BO9vb0jzQ4sWn8WrCYyHkURX27RBC/uulECLyHizDxeFrHRyFvswILVJLgwDM2llL8AgK2bHMrdmcCaBGqIeIwQ4jHGkpwAC1ZyVq/0nDdv3uvGjBlzLQAc3sJwHlJsAqbe+gVLliw5P8+119NKMQtWG2S11p8EgMvaMMFDi0XgUQA4gb8FbD3pLFits/vnyFqttlsQBD8DAPNtIjcmsC4Co4j4nVWrVn25p6dnJSNqnQALVuvs1hoZhuFMRLwIADbvkEk2kw8C9wdBcHKlUlmYj3CyjYIFq4P8+/v7/2NkZMTU13pnB82yKT8JrCKi8x5++OGLeK+qcwlkweocy1csNW7oMXtbe6Vgnk06ToCIbgqC4DP8DWDnE8WC1Xmm/7Q4Z86c8sSJEz+KiOcCwBYpTcNm3SLQH8fxx4pavthGKliwUqbc19e3ZVdX13kAcBq/k5gy7OzMP4WI51QqlZ8iImXnRv5nZsGylGOt9e6IaG7rOZ5ryFuCnvI0iPhcHMcXA8DlSqkVKU/H5rkelv01UKvV9kTECxDxaOZvn3+HZnyBiC4bN27cpVOmTHmxQzbZTAIC/ISVAFIaXbTW+wHA+QDw7jTss81UCAwS0RWlUunCSqXyQiozsNENEmDByniBmI+KAHA2AJwEAOMydoenXzcB877fd7q6umbxE1W2S4QFK1v+r8weRZGpt3UGEX0MALZyxK2iu1EDgEuWLFlyE5+lcmMpsGC5kYdXvOjr6xs/duzYExHxVABQjrlXFHeuI6KrlFJzixKwL3GyYDmcqTAM34iI5jjEiQCwmcOu5sE1c/ff1QBwjVJqeR4CymMMLFgeZHWNp64TzO2/Hrjsi4tm4/wGRLxGCPEnX5wusp8sWJ5l37yvODo6elzjPNcBfDSi6QSaC0lvieP4ekS8g+/8a5pfpgNYsDLF397kCxYs2LFUKh2PiG8hogMBYEJ7FnM7eikA3ImIty9fvvz23t7eVbmNNOeBsWDlJMFhGI4plUpVIjqUiHoBwDx9jc9JeM2GYQTqbvNKZ7lcvru7u/uRZg1wfzcJsGC5mZe2vVq0aNHYoaGh/YnoTYhobrE2ApbXJ7DHiOieIAjuGR0dvbtarT7cNkA24CQBFiwn05KOU2EYzjCVURHRnLLfGwC605kpPauI+A8ieoCIFiFiVC6X7+EnqPR4u2aZBcu1jFj2p3HS3oiXqd01GQB2BoCdGn9uYtmdl6czNyCb0+WPI+LDcRwvIaIHEXERHznIKCOOTMuC5UgiXHRj/vz5W5VKJSNg2zWuNds6CIKt4zg2p/K3RkRTDnrTxl6Z+bhpfsy+2cRGPAMAYKoYvNT4cwUiDsRxvAwAXvlBxGXmySmO478ppZa4yIJ9coMAC5YbeWAvmAATSECABSsBJO7CBJiAGwRYsNzIA3vBBJhAAgIsWAkgcRcmwATcIMCC5UYe2AsmwAQSEGDBSgCJuzABJuAGARYsN/LAXjABJpCAAAtWAkjchQkwATcIsGC5kQf2ggkwgQQEWLASQOIuTIAJuEGABcuNPLAXTIAJJCDAgpUAEndhAkzADQIsWG7kgb1gAkwgAQEWrASQuAsTYAJuEGDBciMP7AUTYAIJCLBgJYDEXZgAE3CDAAuWG3lgL5gAE0hAgAUrASTuwgSYgBsEWLDcyAN7wQSYQAICLFgJIHEXJsAE3CDAguVGHtgLJsAEEhBgwUoAibswASbgBgEWLDfywF4wASaQgAALVgJI3IUJMAE3CLBguZEH9oIJMIEEBP4PcHF64ZkIhX4AAAAASUVORK5CYII='
              }
              if (data.data.data.audio.title.length >= 11){
                data.data.data.audio.title = data.data.data.audio.title.substr(0, 11) + " ...";
              }
              if (data.data.data.audio.author.length >= 13) {
                data.data.data.audio.author = data.data.data.audio.author.substr(0, 13) + " ...";
              }
            }
            
            that.setData({
              shareNumbers: data.data.data.article.shareNumbers,
              commitTime: new Date(data.data.data.article.commitTime).Format("yyyy-MM-dd"),
              author: (data.data.data.createPeople.firstName + " " + data.data.data.createPeople.lastName),
              serverData: data.data.data,
              likeImageUrl: ((data.data.data.thumbsUp == true) ? "/image/articleDetails/ThumbsUp.png" : "/image/articleDetails/ThumbsUp_Disabled.png"),
              favoritesUrl: ((data.data.data.weatherFollow == true) ? "/image/articleDetails/Favorites_Enable.png" : "/image/articleDetails/Favorites.png"),
              likeCount: data.data.data.article.likeIds.length,
              isShowSignUp: data.data.data.article.enrollOrVote
            },function(){
                //跳转到文章对于的位置。
                if (wx.pageScrollTo) {
                  setTimeout(function () {
                    var articleId = data.data.data.article.id;
                    if (wx.getStorageSync("browseAddress") != '') {
                      if (wx.getStorageSync("browseAddress").split('_')[0] == articleId) {
                        wx.pageScrollTo({
                          scrollTop: wx.getStorageSync("browseAddress").split('_')[1]
                        });
                      }
                    }
                  }, 200);
                }

                if (wx.getStorageSync("zoom") != "") {
                  that.setData({
                    zoom: wx.getStorageSync("zoom")
                  });
                }
            });
          }
        },
        fail: function (err) {
          that.setData({ showWhetherBind: true });
          wx.hideNavigationBarLoading(); wx.hideLoading();
        }
      })
  },
  openActivitiesDetails: function (e) {
    var param = e;
    wx.navigateTo({
      url: '/pages/articleList/articleDetails/articleDetails?id=' + e.currentTarget.dataset.id + "&likeCount=0&title=" + e.currentTarget.dataset.title,
      fail: function(err){
        wx.redirectTo({
          url: '/pages/articleList/articleDetails/articleDetails?id=' + param.currentTarget.dataset.id + "&likeCount=0&title=" + param.currentTarget.dataset.title
        });
      }
    });
  },
  setFontSize: function(e){
    var flag = e.currentTarget.dataset.flag;
    if (flag == "+"){
      if (this.data.zoom < 1.8){
        this.data.zoom = parseFloat((this.data.zoom + 0.1).toFixed(1))
        this.setData({ zoom: this.data.zoom});
      }else{
        this.setData({ zoom: 1.8 });
      }
    } else if (flag == "-"){
      if (this.data.zoom > 1) {
        this.data.zoom = parseFloat((this.data.zoom - 0.1).toFixed(1))
        this.setData({ zoom: this.data.zoom});
      } else {
        this.setData({ zoom: 1});
      }
    }
    wx.setStorageSync("zoom", this.data.zoom);
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  },
  //以下是报名模块（初始化数据）
  btnSignUp: function(){
    var that = this;
    wx.showLoading({ title: '加载中...', mask: true });
    API.whetherEnroll({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      aId: this.data.id,
      success: function (json) {
        wx.hideLoading();
        if (json.data.code == 1) {
          if (json.data.data.openId == undefined){
              // 请不要重复报名！
              wx.showModal({
                title: '提示：',
                content: '请不要重复报名！',
                showCancel: false
              });
          }else{
              var relationshipStr = "";
              switch (json.data.data.relationship){
                case "father":
                  relationshipStr = "父亲Father";
                  break;
                case "mother":
                  relationshipStr = "母亲Mother";
                  break;
                case "sibling":
                  relationshipStr = "兄弟姐妹Sibling";
                  break;
                case "grandparents":
                  relationshipStr = "祖父母Grandparents";
                  break;
              }
              that.setData({
                showViewSignUp: "block",
                relationshipStr: relationshipStr,
                registrationData: {
                  aId: that.data.id,
                  pId: json.data.data.id,
                  nickName: json.data.data.nickName,
                  gender: json.data.data.gender,
                  city: json.data.data.city,
                  province: json.data.data.province,
                  country: json.data.data.country,
                  avatarUrl: json.data.data.avatarUrl,
                  phones: [json.data.data.phones[0]],
                  enrollphone: json.data.data.phones[0],
                  relationship: json.data.data.relationship,
                  openId: json.data.data.openId,
                  sIds: json.data.data.sIds
                }
              });
          }
        }
      },
      fail: function (err) {
        wx.hideLoading();
      }
    });
  },
  selectSex: function(e){
    this.setData({
      registrationData: {
        ...this.data.registrationData,
        gender: e.detail.value
      }
    });
  },
  bindSignUpSubmit: function(e){
    if (e.detail.value.nickName.trim() == ""){
      wx.showModal({
        title: '提示：',
        content: '对不起，昵称 不能为空。',
        showCancel: false
      });
      return;
    }
    if (e.detail.value.enrollphone.trim() == "") {
      wx.showModal({
        title: '提示：',
        content: '对不起，联系电话 不能为空。',
        showCancel: false
      });
      return;
    }
    var that = this;
    that.data.registrationData.nickName = e.detail.value.nickName;
    that.data.registrationData.enrollphone = e.detail.value.enrollphone;
    wx.showLoading({ title: '提交中...', mask: true });
    API.enroll({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("openId"),
      },
      data: that.data.registrationData,
      success: function (json) {
        if (json.statusCode != 200) {
          wx.showModal({
            title: '提示：',
            content: '对不起，链接断开。',
            showCancel: false,
            success: function (res) {
              wx.hideLoading();
            }
          });
          return;
        }
        if (json.data.code == 1) {
          wx.showModal({
            title: '提示：',
            content: '您的信息已提交成功，工作人员稍后与您联系。',
            showCancel: false,
            success: function (res) {
              wx.hideLoading();
              that.setData({ showViewSignUp: "none" });
            }
          });
        } else {
          wx.showModal({
            title: '提示：',
            content: '对不起，链接断开。',
            showCancel: false,
            success: function (res) {
              wx.hideLoading();
            }
          });
        }
      },
      fail: function (err) {
        wx.hideLoading();
        wx.showModal({
          title: '提示：',
          content: '对不起，链接断开。',
          showCancel: false
        });
      }
    });
  },
  bindSignUpReset: function () {
    this.setData({ showViewSignUp: "none" });
  },
  wxParseTagATap: function (e) {
    console.log(e.currentTarget.dataset.src);
    wx.navigateTo({
      url: '/pages/webView/webView?src=' + encodeURIComponent(e.currentTarget.dataset.src),
      fail: function (err) {
        wx.redirectTo({
          url: '/pages/webView/webView?src=' + encodeURIComponent(e.currentTarget.dataset.src)
        });
      }
    });
  },
  audioError: function(err){
    wx.showModal({
      title: '提示：',
      content: '音频文件，播放失败。',
      showCancel: false
    });
  }
})