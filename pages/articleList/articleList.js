//index.js
import API from '../../api/api';
import utils from '../../utils/util.js';
Page({
  data: {
      serverData: [],
      type: 1,
      title: "",
      pageSize: 10,
      totalPage: 0,
      viewAndroid: "",
      platform: "",
      minHeight: "0px",
      isLoading: true,
      isError: false,
      isFirst: true
  },
  btnUnFolllow: function(e){
    var that = this;
    wx.showModal({
      title: '提示:',
      content: '你确定要 取消收藏 吗？',
      success: function (res) {
        if (res.confirm) {
            API.weatherFollow({
              method: "POST",
              header: {
                'Content-Type': 'application/json',
                userAuth: wx.getStorageSync("openId"),
              },
              aId: e.currentTarget.dataset.aid,
              weatherFollow: "unFolllow",
              success: function (data) {
                if (data.data.code == 1) {
                  wx.showToast({
                      title: '取消收藏',
                      icon: 'success',
                      duration: 2000
                  });
                  that.onShow();
                }
              }
            });
        }
      }
    });
  },
  onImgLoad: function(e){
      this.data.serverData[e.currentTarget.id].bgPicture = "background-image:none";
      this.setData({
          serverData: this.data.serverData
      });
  },
  onPullDownRefresh: function(){
      if (this.data.isFirst == true){
        this.setData({ isLoading: true, isError: false });
      }
      this.onRefreshData(this.data.type);
      wx.stopPullDownRefresh();
  },
  onReachBottom: function(){
    if (this.data.totalPage > 0 && this.data.pageSize < this.data.totalPage * this.data.pageSize) {
        this.data.pageSize+=10;
        this.onRefreshData(this.data.type);
    }
  },
  onLoad: function(param){
      wx.setNavigationBarTitle({ title: param.title });
      this.setData({ title: param.title, type: param.type, isLoading: true, isError:false});
      this.onRefreshData(param.type);
      var that = this;
      wx.getSystemInfo({
        success: function (res) {
          if (res.platform == "android" || res.platform == "devtools"){
            that.setData({ platform: res.platform, viewAndroid: "viewFlap viewAndroid", minHeight: res.windowHeight + "px"});
          }else{
            that.setData({ platform: res.platform, viewAndroid: "viewFlap1", minHeight: res.windowHeight + "px"});
          }
        }
      })
  },
  onRefreshData: function(typeStr){
      var that = this;
      if (typeStr != undefined) {
          API.findArticles({
            method: "POST",
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("openId"),
            },
            data: {
              conditions: {
                app: "app",
                type: typeStr
              },
              pageNumber: 1,
              pageSize: this.data.pageSize,
              sort: "publishDate", //publishDate
              dir: "DESC"
            },
            success: function (data) {
              if (data.data.code == 1) {
                for (var i = 0; i < data.data.data.content.length; i++) {
                  data.data.data.content[i].publishDate = new Date(data.data.data.content[i].publishDate).Format("yyyy-MM-dd");
                  data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
                }
                for (var i = 0; i < that.data.serverData.length;i++){
                  if (that.data.serverData[i].bgPicture != undefined){
                    if (data.data.data.content[i] != undefined) {
                      data.data.data.content[i].bgPicture = that.data.serverData[i].bgPicture;
                    }else{
                      break;
                    }
                  }
                }
                that.setData({
                  serverData: data.data.data.content,
                  totalPage: data.data.data.totalPage,
                  isLoading: false,
                  isError: false,
                  isFirst: false
                })
              }
            },
            fail: function (err) {
              that.setData({ isLoading: false, isError: true});
            }
          });
      } else {
          API.follows({
            method: "POST",
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("openId"),
            },
            data: {
              conditions: {},
              pageNumber: 1,
              pageSize: this.data.pageSize,
              sort: "publishDate",
              dir: "DESC"
            },
            success: function (data) {
              if (data.data.code == 1) {
                for (var i = 0; i < data.data.data.content.length; i++) {
                  data.data.data.content[i].publishDate = new Date(data.data.data.content[i].publishDate).Format("yyyy-MM-dd");
                  data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
                }
                for (var i = 0; i < that.data.serverData.length; i++) {
                  if (that.data.serverData[i].bgPicture != undefined) {
                    if (data.data.data.content[i] != undefined) {
                        data.data.data.content[i].bgPicture = that.data.serverData[i].bgPicture;
                    }else{
                        break;
                    }
                  }
                }
                that.setData({
                  serverData: data.data.data.content,
                  totalPage: data.data.data.totalPage,
                  isLoading: false,
                  isError: false,
                  isFirst: false
                })
              }
            },
            fail: function (err) {
              that.setData({ isLoading: false, isError: true});
            }
          });
      }
  },
  onShow: function(){
    var that = this;
    if (this.data.title == "收藏夹"){
        API.follows({
          method: "POST",
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("openId"),
          },
          data: {
            conditions: {},
            pageNumber: 1,
            pageSize: this.data.pageSize,
            sort: "publishDate",
            dir: "DESC"
          },
          success: function (data) {
            if (data.data.code == 1) {
              for (var i = 0; i < data.data.data.content.length; i++) {
                data.data.data.content[i].publishDate = new Date(data.data.data.content[i].publishDate).Format("yyyy-MM-dd");
                data.data.data.content[i].summaryLength = data.data.data.content[i].summary.length;
              }
              for (var i = 0; i < data.data.data.content.length; i++) {
                if (that.data.serverData[i].bgPicture != undefined) {
                  if (data.data.data.content[i] != undefined) {
                      data.data.data.content[i].bgPicture = that.data.serverData[i].bgPicture;
                  }else{
                      break;
                  }
                }
              }
              that.setData({
                serverData: data.data.data.content,
                totalPage: data.data.data.totalPage,
                isLoading: false,
                isError: false,
                isFirst: false
              });
            }
          }, 
          fail: function (err) {
            that.setData({ isLoading: false, isError: true });
          }
        });
    }else{
        if (this.data.isFirst == false){
          this.setData({ isLoading: false, isError: false });
          this.onRefreshData(this.data.type);
        }
    }
  },
  openActivitiesDetails: function(e){
      wx.navigateTo({
        url: '/pages/articleList/articleDetails/articleDetails?id=' + e.currentTarget.dataset.id + "&likeCount=" + e.currentTarget.dataset.likecount + "&title=" + this.data.title
      });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
